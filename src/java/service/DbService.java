/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.SQLException;
import java.util.Vector;
import javax.servlet.http.HttpServlet;

/**
 *
 * @author Fai
 */
public abstract class DbService<T> {
    final protected HttpServlet servlet;
    
    public DbService(HttpServlet servlet){
        this.servlet = servlet;
    }
    
    public abstract T insert(T t) throws SQLException;
    public abstract int update(T t) throws SQLException;
    public abstract int delete(T t) throws SQLException;
    public abstract T query(T t) throws SQLException;
    public abstract Vector<T> all() throws SQLException;
}
