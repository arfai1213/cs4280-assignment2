/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import bean.Toy;
import bean.User;
import impl.ToyService;
import java.io.IOException;
import java.util.Vector;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import util.SessionChecker;
/**
 *
 * @author Fai
 */
public class MaintainToyServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();

        response.setContentType("text/html;charset=UTF-8");
        String action = request.getParameter("action");
        String toyID = request.getParameter("id");
        if(action!=null && action.equals("remove")){
            
            try{
                ToyService toyService = new ToyService(this);
                Toy toy = new Toy();
                toy.setToyID(Integer.parseInt(toyID));
                toyService.delete(toy);
                
                Vector<Toy> toys = toyService.allNonSecondHand(null,null,null,null);

                request.setAttribute("toys", toys);
                request.setAttribute("action", "view");
                session.setAttribute("msg", "Removed.");
                RequestDispatcher view = request.getRequestDispatcher("maintain-toys.jsp");
                view.forward(request, response);
            }catch(Exception ex){
                ex.printStackTrace();
            }
        }else{
            try{
                ToyService toyService = new ToyService(this);
                Vector<Toy> toys = toyService.allNonSecondHand(null,null,null,null);

                request.setAttribute("toys", toys);
                request.setAttribute("action", "view");
                RequestDispatcher view = request.getRequestDispatcher("maintain-toys.jsp");
                view.forward(request, response);
            }catch(Exception ex){
                ex.printStackTrace();
            }
        } 
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User loginUser = SessionChecker.getCurrentUser(session);
        
        if(loginUser == null || loginUser.isCustomer()){
            //Is customer or not yet login
            response.sendError(401);
            return;
        }
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        processRequest(request, response);
        //TODO save-edit
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
