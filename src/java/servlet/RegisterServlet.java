/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import bean.User;
import impl.UserService;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Allen
 */
public class RegisterServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processPostRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = request.getParameter("action");

        if ("register".equals(action)) {
            UserService userSrv = new UserService(this);
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            String passwordRetype = request.getParameter("password-retype");
            String fullname = request.getParameter("fullname");
            String address = request.getParameter("address");
            HttpSession session = request.getSession(true);
            if(fullname.equals("") ||username.equals("") || password.equals("") || address.equals("")) {
                session.setAttribute("msg", "Register failure, please enter all information.");
                response.sendRedirect("register.jsp");
            } else if (!passwordRetype.equals(password)) {
                session.setAttribute("msg", "Register failure, password retype not match.");
                response.sendRedirect("register.jsp");
            } else {
                User user = new User();
                user.setUsername(username);
                user.setPassword(password);
                user.setDisplayName(fullname);
                user.setAddress(address);
                user.setRoleID("C");
                try {
                    User createdUser= userSrv.insert(user);
                    if (createdUser == null) {
                        session.setAttribute("msg", "username was used by someone else");
                        response.sendRedirect("register.jsp");
                    }else{
                        session.setAttribute("loginUser", createdUser);
                        response.sendRedirect("home");
                    }
                }catch(Exception ex){
                    session.setAttribute("msg", "Register failure, invalid information");
                    response.sendRedirect("register.jsp");
                }
            }
            
        }else {
            response.sendRedirect("homepage.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processPostRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
