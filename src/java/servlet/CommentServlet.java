/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import bean.Comment;
import bean.User;
import impl.CommentService;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Fai
 */
public class CommentServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String content = request.getParameter("comment");
        int toyID = Integer.parseInt(request.getParameter("toyID"));
        Integer replyTo = null ;
        Integer userId = null;
        
        if(request.getParameter("reply") != null && !request.getParameter("reply").equals("")){
            replyTo = Integer.parseInt(request.getParameter("reply"));
        }
        
        HttpSession session = request.getSession();
        User loginUser = (User)session.getAttribute("loginUser");
        if(loginUser != null){
            userId = loginUser.getUserID();
        }        
        
        if("add".equals(request.getParameter("action"))){
            Comment cmt = new Comment();
            cmt.setContent(content);
            cmt.setReplyTo(replyTo);
            cmt.setToyID(toyID);
            
            try{
                cmt.setUserId(userId);
                
                CommentService cmtSrv = new CommentService(this);
                cmtSrv.insert(cmt);
                session.setAttribute("msg", "Comment added");
            }catch(Exception ex){
                ex.printStackTrace();
            }
            
            //redirect
            response.sendRedirect("toy?id=" + toyID);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
