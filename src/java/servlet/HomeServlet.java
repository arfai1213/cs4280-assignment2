/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import bean.Toy;
import impl.ToyService;
import java.io.IOException;
import java.util.Vector;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Fai
 */
public class HomeServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        RequestDispatcher view = null;
        
                
        try{
            ToyService toyService = new ToyService(this);
            Vector<Toy> toys = toyService.allForSold(null,null,null,null);
            Vector<Toy> firstHandToys = toyService.allNonSecondHand(null,null,null,null);
            Vector<Toy> secondHandToys = toyService.allApprovedSecondHand(null,null,null,null);
            request.setAttribute("toys", toys);
            request.setAttribute("firstHandToys", firstHandToys);
            request.setAttribute("secondHandToys", secondHandToys);
            
            view = request.getRequestDispatcher("homepage.jsp");
            view.forward(request, response);
        }catch(Exception ex){
            ex.printStackTrace();
        }
        
        /*if(SessionChecker.isLogin(session)){
            // already login, can be forwarded to homepage
            view = request.getRequestDispatcher("homepage.jsp");
        }else{
            // else forward to login page
            view = request.getRequestDispatcher("login.jsp");
        }
        
        view.forward(request, response);*/
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
