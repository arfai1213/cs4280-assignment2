/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import bean.Order;
import bean.User;
import impl.OrderService;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import util.SessionChecker;

/**
 *
 * @author Fai
 */
public class OrderServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        String orderID = request.getParameter("id");
        OrderService orderSrv = new OrderService(this);
        HttpSession session = request.getSession();

        if (!SessionChecker.isLogin(session)) {
            session.setAttribute("msg", "Please login first.");
            response.sendRedirect("home");
            return;
        }

        if (orderID == null) {
            User loginUser = SessionChecker.getCurrentUser(session);

            if (!loginUser.isCustomer()) { //is manager
                Vector<Order> allOrders = orderSrv.all();
                Map<String, Integer> reportData = orderSrv.getBrandReport();

                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);

                double totalSaleThisMonth = orderSrv.getTotalSaleAmountByMonth(year, month + 1);

                request.setAttribute("allOrders", allOrders);
                request.setAttribute("brandReportData", reportData);
                request.setAttribute("totalSales", totalSaleThisMonth);

                RequestDispatcher view = request.getRequestDispatcher("admin/allOrders.jsp");
                view.forward(request, response);
            }else{
                //customer can only view its own placed order
                Vector<Order> allCustOrders = orderSrv.queryByUser(loginUser);
                request.setAttribute("allOrders", allCustOrders);
                RequestDispatcher view = request.getRequestDispatcher("customer/allCustOrders.jsp");
                view.forward(request, response);
            }
        } else {
            int id = Integer.parseInt(request.getParameter("id"));

            Order order = new Order();
            order.setOrderID(id);
            order = orderSrv.query(order);

            request.setAttribute("order", order);

            RequestDispatcher view = request.getRequestDispatcher("single-order.jsp");
            view.forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User loginUser = SessionChecker.getCurrentUser(session);
        
        if(loginUser == null){
            //Is customer or not yet login
            response.sendError(401);
            return;
        }
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(OrderServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User loginUser = SessionChecker.getCurrentUser(session);
        
        if(loginUser == null){
            //Is customer or not yet login
            response.sendError(401);
            return;
        }
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(OrderServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
