/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import bean.Order;
import bean.Orderline;
import bean.Toy;
import bean.User;
import impl.OrderService;
import impl.ToyService;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import util.SessionChecker;

/**
 *
 * @author Fai
 */
public class CartServlet extends HttpServlet {

    private final String CART_SESSION_NAME = "cart";

    public void addToCart(ArrayList<Orderline> list, Orderline ol) {
        if (list == null) {
            return;
        }

        //replace toy if exist
        boolean isExist = false;
        for (int i = 0; i < list.size(); i++) {
            Orderline existOl = list.get(i);

            if (ol.getToy().getToyID() == existOl.getToy().getToyID()) {
                isExist = true;
                existOl.setQty(ol.getQty());
                break;
            }
        }

        if (!isExist) {
            list.add(ol);
        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        HttpSession session = request.getSession();
        ArrayList<Orderline> sessionList = null;
        sessionList = (ArrayList<Orderline>) session.getAttribute(this.CART_SESSION_NAME);

        if ("add".equals(action)) {
            // add toy to shopping cart
            int toyID = Integer.parseInt(request.getParameter("id"));
            int qty = Integer.parseInt(request.getParameter("qty"));

            ToyService toySrv = new ToyService(this);
            Toy toy = new Toy();
            Orderline ol = new Orderline();
            ol.setQty(qty);
            ol.setToyID(toyID);

            try {
                toy.setToyID(toyID);
                toy = toySrv.query(toy);

                ol.setToy(toy);
            } catch (SQLException ex) {
                Logger.getLogger(CartServlet.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (sessionList == null) {
                //first time to add
                sessionList = new ArrayList<Orderline>();
            }

            addToCart(sessionList, ol);
            session.setAttribute(this.CART_SESSION_NAME, sessionList);
            session.setAttribute("msg", "Toy added to shopping cart");

            response.sendRedirect("toy?id=" + toy.getToyID());
        } else {
            if ("remove".equals(action)) {
                //remove toy from cart
                int toyID = Integer.parseInt(request.getParameter("id"));

                if (sessionList != null) {
                    for (int i = 0; i < sessionList.size(); i++) {
                        Orderline ol = sessionList.get(i);
                        if (ol.getToy().getToyID() == toyID) {
                            sessionList.remove(ol);
                            break;
                        }
                    }
                    session.setAttribute(this.CART_SESSION_NAME, sessionList);
                }
            }

            //show cart            
            RequestDispatcher view = request.getRequestDispatcher("shopping-cart.jsp");
            view.forward(request, response);
        }
    }

    public void createOrder(HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException {
        String[] toys = request.getParameterValues("toyID[]");
        String[] qtys = request.getParameterValues("qty[]");
        ToyService toySrv = new ToyService(this);

        Order order = new Order();
        for (int i = 0; i < toys.length; i++) {
            Orderline ol = new Orderline();
            int qty = Integer.parseInt(qtys[i]);
            int toyID = Integer.parseInt(toys[i]);

            ol.setQty(qty);
            ol.setToyID(toyID);

            Toy toy = new Toy();
            toy.setToyID(toyID);
            toy = toySrv.query(toy);

            ol.setSubtotal(qty * toy.getPrice());
            ol.setToy(toy);
            //calculate subtotal

            order.addOrderlines(ol);
        }

        order.setAddr(request.getParameter("deliveryAddr"));
        HttpSession session = request.getSession();
        User user = SessionChecker.getCurrentUser(session);
        order.setOrderBy(user.getUserID());

        OrderService orderSrv = new OrderService(this);
        order = orderSrv.insert(order);
        //remove all items from cart 
        //and redirect to view the order

        session.removeAttribute(this.CART_SESSION_NAME);
        session.setAttribute("msg", "Order created successfully!");
        
        response.sendRedirect("order?id=" + order.getOrderID());
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            createOrder(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(CartServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
