/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import bean.Toy;
import bean.User;
import impl.ToyService;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileItemFactory;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import util.SessionChecker;
/**
 *
 * @author Fai
 */
public class CreateToyServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendRedirect("create-new-toy.jsp");
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User loginUser = SessionChecker.getCurrentUser(session);
        
        if(loginUser == null || loginUser.isCustomer()){
            //Is customer or not yet login
            response.sendError(401);
            return;
        }
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User loginUser = SessionChecker.getCurrentUser(session);
        
        if(loginUser == null || loginUser.isCustomer()){
            //Is customer or not yet login
            response.sendError(401);
        }
        //response.setContentType("text/html;charset=UTF-8");
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        String header = "";
        String description = "";
        String sex = "";
        String age = "";
        String brand = "";
        String price = "";
        String image = "";
        if (isMultipart)
        {
            try 
            {
                
                DiskFileItemFactory factory = new DiskFileItemFactory();



        
                
                ServletFileUpload upload = new ServletFileUpload( factory );
                List items = upload.parseRequest(request);
                
                Iterator iterator = items.iterator();
                while (iterator.hasNext()) 
                {
                    DiskFileItem item = (DiskFileItem) iterator.next();

                    if (item.isFormField()) //your code for getting form fields
                    {
                        String name = item.getFieldName();
                        String value = item.getString();
                        if(name.equals("header")) {
                            header = value;
                        } else if(name.equals("description")) {
                            description = value;
                        } else if(name.equals("sex")) {
                            if(sex.equals("")) {
                                sex = value;
                            } else {
                                sex = sex + "/" + value;
                            }
                            
                        } else if(name.equals("age")) {
                            age = value;
                        } else if(name.equals("brand")) {
                            brand = value;
                        } else if(name.equals("price")) {
                            price = value;
                        } else if(name.equals("image")) {
                            image = value;
                        }
                    }

                }         
                if(header.equals("") || description.equals("") || sex.equals("") || age.equals("") || brand.equals("") ||image.equals("")||image.equals("")) {
                    throw new Exception("Input invalid");
                }          
                ToyService toyService = new ToyService(this);
                Toy toy = new Toy();
                toy.setHeader(header);
                toy.setDescription(description);
                toy.setSex(sex);
                toy.setAge(age);
                toy.setBrand(brand);
                toy.setPrice(Double.parseDouble(price));
                toy.setImage(image);

                toyService.insert(toy);
                session.setAttribute("msg", "Created.");
                response.sendRedirect("maintain-toys");
            } catch (SQLException ex) {
                Logger.getLogger(EditToyServlet.class.getName()).log(Level.SEVERE, null, ex);
                session.setAttribute("msg", "Error occur while inserting");
                response.sendRedirect("create-toy");
            } catch (FileUploadException ex) {
                Logger.getLogger(EditToyServlet.class.getName()).log(Level.SEVERE, null, ex);
                session.setAttribute("msg", "Fail Upload image");
                response.sendRedirect("create-toy");
            } catch (NumberFormatException e) {
                session.setAttribute("msg", "Input Invalid");
                response.sendRedirect("create-toy");
            } catch (FileNotFoundException ex) {
                Logger.getLogger(EditToyServlet.class.getName()).log(Level.SEVERE, null, ex);
                session.setAttribute("msg", "Please select file");
                response.sendRedirect("create-toy");
            } catch (Exception ex) {
                Logger.getLogger(EditToyServlet.class.getName()).log(Level.SEVERE, null, ex);
                session.setAttribute("msg", ex.getMessage());
                response.sendRedirect("create-toy");
            }
        
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
