/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

/**
 *
 * @author Fai
 */
public class Toy {
    private int toyID;
    private String header;
    private String description;
    private String sex;
    private String age;
    private String brand;
    private double price;
    private String image;
    private boolean isDeleted = false;
    private SecondHandToy secondHandToy;
    public boolean isDeleted(){
        return isDeleted;
    }
    public void setIsDeleted(boolean b){
        isDeleted = b;
    }
    public int getToyID() {
        return toyID;
    }

    public void setToyID(int toyID) {
        this.toyID = toyID;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public SecondHandToy getSecondHandToy() {
        return secondHandToy;
    }

    public void setSecondHandToy(SecondHandToy secondHandToy) {
        this.secondHandToy = secondHandToy;
    }
    
    public boolean isSecondHand(){
        if(this.secondHandToy == null)
            return false;
        return true;
    }
    
    @Override
    public String toString() {
        return "Toy{" + "toyID=" + toyID + ", header=" + header + ", description=" + description + ", sex=" + sex + ", age=" + age + ", brand=" + brand + ", price=" + price + ", image=" + image + '}';
    }
    
    
    
}
