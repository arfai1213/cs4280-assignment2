/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import java.util.Date;

/**
 *
 * @author Fai
 */
public class SecondHandToy {
    private int toyID;
    private Toy toy;
    private boolean approved;
    private Date createDate;
    private User soldBy;
    private int soldByCust;
    private boolean isSold;
    
    public static final String WAIT_APPROVE = "Waiting approve";
    public static final String SELLING = "Selling";
    public static final String SOLD = "Sold";

    public int getToyID() {
        return toyID;
    }

    public void setToyID(int toyID) {
        this.toyID = toyID;
    }
    
    public Toy getToy() {
        return toy;
    }

    public void setToy(Toy toy) {
        this.toy = toy;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public User getSoldBy() {
        return soldBy;
    }

    public void setSoldBy(User soldBy) {
        this.soldBy = soldBy;
    }

    public int getSoldByCust() {
        return soldByCust;
    }

    public void setSoldByCust(int soldByCust) {
        this.soldByCust = soldByCust;
    }

    @Override
    public String toString() {
        return "SecondHandToy{" + "toyID=" + toyID + ", toy=" + toy + ", approved=" + approved + ", createDate=" + createDate + ", soldBy=" + soldBy + ", soldByCust=" + soldByCust + '}';
    }

    public boolean isSold() {
        return isSold;
    }

    public void setIsSold(boolean isSold) {
        this.isSold = isSold;
    }
    
    public String getStatus(){
        if(this.isSold())
            return SecondHandToy.SOLD;
        
        if(this.isApproved())
            return SecondHandToy.SELLING;
        
        return SecondHandToy.WAIT_APPROVE;
    }
     
    
}
