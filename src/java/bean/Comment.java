/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import java.util.Date;
import java.util.Vector;

/**
 *
 * @author Fai
 */
public class Comment {
    private Integer commentID;
    private String content;
    private Integer toyID;
    private Integer replyTo;
    private Date date;
    private Integer userId;
    private Vector<Comment> replies;
    private User replyToUser;
    
    public Integer getCommentID() {
        return commentID;
    }

    public void setCommentID(Integer commentID) {
        this.commentID = commentID;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getToyID() {
        return toyID;
    }

    public void setToyID(Integer toyID) {
        this.toyID = toyID;
    }

    public Integer getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(Integer replyTo) {
        this.replyTo = replyTo;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getDisplayName() {
        if(this.replyToUser == null){
            return "Visitor";
        }
        return this.replyToUser.getDisplayName();
    }
    
    public String getHTMLDisplayName(){
        String value = "<i class=\"fi-torso\"></i>&nbsp;";
        if(this.getDisplayName().equals("Visitor")){
            return this.getDisplayName();
        }else{
            return value + this.getDisplayName();
        }
    }

    public Vector<Comment> getReplies() {
        return replies;
    }

    public void setReplies(Vector<Comment> replies) {
        this.replies = replies;
    }

    public User getReplyToUser() {
        return replyToUser;
    }

    public void setReplyToUser(User replyToUser) {
        this.replyToUser = replyToUser;
    }

    
}
