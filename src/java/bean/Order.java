/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import java.util.Date;
import java.util.Vector;

/**
 *
 * @author Fai
 */
public class Order {
    private int orderID;
    private User orderByUser;
    private int orderBy;
    private String addr;
    private Date orderDate;
    private Vector<Orderline> orderlines = new Vector<Orderline>();

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public User getOrderByUser() {
        return orderByUser;
    }

    public void setOrderByUser(User orderByUser) {
        this.orderByUser = orderByUser;
    }

    public int getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(int orderBy) {
        this.orderBy = orderBy;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public Vector<Orderline> getOrderlines() {
        return orderlines;
    }

    public void addOrderlines(Orderline ol) {
        this.orderlines.add(ol);
    }
    
    public double getTotal(){
        double total = 0;
        for(int i = 0; i < this.orderlines.size(); i++){
            total += orderlines.get(i).getSubtotal();
        }
        
        return total;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }
    
    public int getNumberOfToysPurchased(){
        int num = 0;
        for(int i = 0; i < this.orderlines.size(); i++){
            Orderline ol = orderlines.get(i);
            num += ol.getQty();
        }
        
        return num;
    }
}
