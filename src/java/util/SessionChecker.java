/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import bean.User;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Fai
 */
public class SessionChecker {
    public static boolean isLogin(HttpSession session){
        User user = (User)session.getAttribute("loginUser");
        if(user == null)
            return false;
        return true;
    }
    
    public static User getCurrentUser(HttpSession session){
        if(SessionChecker.isLogin(session) == false)
            return null;
        
        User user = (User)session.getAttribute("loginUser");
        return user;
    }
   
}
