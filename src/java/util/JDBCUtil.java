/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Fai
 */
public class JDBCUtil {
    
    public static Connection getConnection(HttpServlet servlet) throws SQLException{
        String jdbcURL = servlet.getServletContext().getInitParameter("dbURL");
        String user = servlet.getServletContext().getInitParameter("dbUser");
        String password = servlet.getServletContext().getInitParameter("dbPassword");
        
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(JDBCUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        Connection con = DriverManager.getConnection(jdbcURL, user, password);
        
        return con;
    }
    
    public static ResultSet query(Connection conn, String sql) throws SQLException{
        Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        ResultSet rs = stmt.executeQuery(sql);
        
        return rs;
    }
   
}
