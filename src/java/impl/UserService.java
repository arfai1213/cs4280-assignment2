/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package impl;

import bean.User;
import java.sql.*;
import java.util.Vector;
import javax.servlet.http.HttpServlet;
import service.DbService;
import util.JDBCUtil;

/**
 *
 * @author Fai
 */
public class UserService extends DbService<User>{

    public UserService(HttpServlet servlet) {
        super(servlet);
    }

    @Override
    public User insert(User t) throws SQLException {
        Connection conn = JDBCUtil.getConnection(servlet);
        
        if(queryByUsername(t.getUsername())!=null) {
            return null;
        }
        
        
        String query = "INSERT INTO [dbo].[user](username,password,role,displayName,address) VALUES (?, ?, ?, ?, ?)";
        PreparedStatement pstmt = conn.prepareStatement(query);
        pstmt.setString(1,t.getUsername());
        pstmt.setString(2,t.getPassword());
        pstmt.setString(3,t.getRoleID());
        pstmt.setString(4,t.getDisplayName());
        pstmt.setString(5,t.getAddress());
        int rows = pstmt.executeUpdate();
        if (rows > 0) {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT @@IDENTITY AS [@@IDENTITY]");
            if (rs != null && rs.next() != false) {
                
                t = queryById(""+rs.getInt(1));
                rs.close();
            }
            return t;
        } else {
            
            return null;
        }
         
    }

    @Override
    public int update(User t) throws SQLException {
        Connection conn = JDBCUtil.getConnection(servlet);
        
        if(t == null)
            return 0;
        
        
        String query = "UPDATE [dbo].[user] SET username = ? , password = ?, role = ?, displayName = ?,address =?, credit = ?) WHERE userID = ?";
        PreparedStatement pstmt = conn.prepareStatement(query);
        pstmt.setString(1,t.getUsername());
        pstmt.setString(2,t.getPassword());
        pstmt.setString(3,t.getRoleID());
        pstmt.setString(4,t.getDisplayName());
        pstmt.setString(5,t.getAddress());
        pstmt.setDouble(6,t.getCredit());
        pstmt.setInt(7,t.getUserID());
        int rows = pstmt.executeUpdate();
        return rows;
    }

    @Override
    public int delete(User t) throws SQLException {
        Connection conn = JDBCUtil.getConnection(servlet);
        if(t == null)
            return 0;
        
        String query = "DELETE FROM [dbo].[user] WHERE userID = ?";
        PreparedStatement pstmt = conn.prepareStatement(query);
        pstmt.setDouble(1,t.getUserID());
        int rows = pstmt.executeUpdate();
        return rows;
    }
    
    public User queryById(String id) throws SQLException {
        if(id == null || id.equals(""))
            return null;
        
        Connection conn = JDBCUtil.getConnection(servlet);
        String sql = "select * from [dbo].[user] where userID = '" + id + "'";
        ResultSet rs = JDBCUtil.query(conn, sql);
        
        rs.first();
        
        if(rs.getRow() == 0)
            return null;
        
        User user = setUserData(rs);
        
        rs.close();
        conn.close();
        
        return user;  
    }
    public User queryByUsername(String username) throws SQLException {
        if(username == null || username.equals(""))
            return null;
        
        Connection conn = JDBCUtil.getConnection(servlet);
        String sql = "select * from [dbo].[user] where username = '" + username + "'";
        ResultSet rs = JDBCUtil.query(conn, sql);
        
        rs.first();
        
        if(rs.getRow() == 0)
            return null;
        
        User user = setUserData(rs);
        
        rs.close();
        conn.close();
        
        return user;  
    }
    

    @Override
    public User query(User t) throws SQLException {
        Connection conn = JDBCUtil.getConnection(servlet);
        String sql = "select * from [dbo].[user] where username = '" + t.getUsername()+ "' and password = '" + t.getPassword() + "';";
        ResultSet rs = JDBCUtil.query(conn, sql);
        
        rs.first();
        
        if(rs.getRow() == 0)
            return null;
        User user = setUserData(rs);
        
        rs.close();
        conn.close();
        
        return user;  
    }

    @Override
    public Vector<User> all() throws SQLException {
        Vector<User> users = new Vector<User>();
        
        Connection conn = JDBCUtil.getConnection(servlet);
        ResultSet rs = JDBCUtil.query(conn, "select * from dbo.user");
        
        while(rs.next()){
            User user = setUserData(rs);
            users.add(user);
        }
        
        rs.close();
        conn.close();
        
        return users;    
    }
    
    public User setUserData(ResultSet rs) throws SQLException{
        User user = new User();
        
        user.setUserID(rs.getInt("userID"));
        user.setUsername(rs.getString("username"));
        user.setPassword(rs.getString("password"));
        user.setRoleID(rs.getString("role"));
        user.setDisplayName(rs.getString("displayName"));
        user.setAddress(rs.getString("address"));
        user.setCredit(rs.getDouble("credit"));
        
        return user;
    }
}
