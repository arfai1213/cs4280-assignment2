/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package impl;

import bean.Order;
import bean.Orderline;
import bean.Toy;
import bean.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import javax.servlet.http.HttpServlet;
import service.DbService;
import util.JDBCUtil;

/**
 *
 * @author Fai
 */
public class OrderService extends DbService<Order> {

    public OrderService(HttpServlet servlet) {
        super(servlet);
    }

    @Override
    public Order insert(Order o) throws SQLException {
        Connection conn = JDBCUtil.getConnection(servlet);
        ToyService toySrv = new ToyService(servlet);
        
        
        int orderID = -1;

        conn.setAutoCommit(false);
        //pre checking

        String query = "INSERT INTO [dbo].[order](orderDate,deliveryAddr,orderBy) VALUES (getDate(), ?, ?)";

        PreparedStatement pstmt = conn.prepareStatement(query);
        pstmt.setString(1, o.getAddr());
        pstmt.setInt(2, o.getOrderBy());

        int rows = pstmt.executeUpdate();
        if (rows == 1) {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT @@IDENTITY AS [@@IDENTITY]");

            if (rs != null && rs.next() != false) {
                Vector<Orderline> ols = o.getOrderlines();
                
                for (int i = 0; i < ols.size(); i++) {
                    //insert orderline
                    orderID = rs.getInt(1);
                    Orderline ol = ols.get(i);
                    query = "INSERT INTO [dbo].[orderline](orderID,toyID,qty, subtotal) VALUES ("
                            + "?,?,?,?)";

                    PreparedStatement pstmt2 = conn.prepareStatement(query);
                    pstmt2.setInt(1, orderID);
                    pstmt2.setInt(2, ol.getToyID());
                    pstmt2.setInt(3, ol.getQty());
                    pstmt2.setDouble(4, ol.getSubtotal());
                    pstmt2.execute();
                    
                    if(ol.getToy().isSecondHand()){
                        //Add credit to that customer
                        int userId = ol.getToy().getSecondHandToy().getSoldByCust();
                        query = "update [dbo].[user] set credit = (credit + ?) where userID = ?";
                        PreparedStatement pstmt3 = conn.prepareStatement(query);
                        pstmt3.setDouble(1, ol.getSubtotal());
                        pstmt3.setInt(2, userId);
                        pstmt3.execute();
                        
                        
                        //Mark that recycled toy to sold
                        query = "update [dbo].[secondhandtoy] set isSold  = ? where toyID = ?";
                        PreparedStatement pstmt4 = conn.prepareStatement(query);
                        pstmt4.setBoolean(1, true);
                        pstmt4.setDouble(2, ol.getToy().getSecondHandToy().getToyID());
                        pstmt4.execute();
                        
                    }
                }
            }

            conn.commit();
            rs.close();
            Order order = new Order();
            order.setOrderID(orderID);
            return this.query(order);
        } else {
            return null;
        }
    }

    @Override
    public int update(Order t) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int delete(Order t) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Order query(Order o) throws SQLException {
        Connection conn = JDBCUtil.getConnection(servlet);
        ResultSet rs = JDBCUtil.query(conn, "select * from [dbo].[order] where orderID = '" + o.getOrderID() + "'");

        rs.first();
        Order order = setOrderData(rs);
        rs.close();

        rs = JDBCUtil.query(conn, "select * from [dbo].[orderline] where orderID = '" + o.getOrderID() + "'");

        while (rs.next()) {
            Orderline ol = new Orderline();
            ol.setOrder(o);
            ol.setOrderID(o.getOrderID());
            ol.setQty(rs.getInt("qty"));
            ol.setSubtotal(rs.getDouble("subtotal"));

            ToyService toySrv = new ToyService(this.servlet);
            Toy toy = new Toy();
            toy.setToyID(rs.getInt("toyID"));
            toy = toySrv.query(toy);

            ol.setToy(toy);
            ol.setToyID(rs.getInt("toyID"));

            order.addOrderlines(ol);
        }

        rs.close();
        conn.close();

        return order;
    }

    @Override
    public Vector<Order> all() throws SQLException {
        Connection conn = JDBCUtil.getConnection(servlet);
        ResultSet rs = JDBCUtil.query(conn, "select * from [dbo].[order] order by orderDate desc");
        Vector<Order> all = new Vector<Order>();

        while (rs.next()) {
            Order order = setOrderData(rs);

            ResultSet rs2 = JDBCUtil.query(conn, "select * from [dbo].[orderline] where orderID = '" + order.getOrderID() + "'");

            while (rs2.next()) {
                Orderline ol = new Orderline();
                ol.setOrder(order);
                ol.setOrderID(order.getOrderID());
                ol.setQty(rs2.getInt("qty"));
                ol.setSubtotal(rs2.getDouble("subtotal"));

                ToyService toySrv = new ToyService(this.servlet);
                Toy toy = new Toy();
                toy.setToyID(rs2.getInt("toyID"));
                toy = toySrv.query(toy);

                ol.setToy(toy);
                ol.setToyID(rs2.getInt("toyID"));

                order.addOrderlines(ol);
            }
            rs2.close();
            
            all.add(order);
        }

        rs.close();
        conn.close();

        return all;
    }
    
    public Vector<Order> queryByUser(User user) throws SQLException {
        Connection conn = JDBCUtil.getConnection(servlet);
        ResultSet rs = JDBCUtil.query(conn, "select * from [dbo].[order] where orderBy = " + user.getUserID());
        Vector<Order> all = new Vector<Order>();

        while (rs.next()) {
            Order order = setOrderData(rs);

            ResultSet rs2 = JDBCUtil.query(conn, "select * from [dbo].[orderline] where orderID = '" + order.getOrderID() + "'");

            while (rs2.next()) {
                Orderline ol = new Orderline();
                ol.setOrder(order);
                ol.setOrderID(order.getOrderID());
                ol.setQty(rs2.getInt("qty"));
                ol.setSubtotal(rs2.getDouble("subtotal"));

                ToyService toySrv = new ToyService(this.servlet);
                Toy toy = new Toy();
                toy.setToyID(rs2.getInt("toyID"));
                toy = toySrv.query(toy);

                ol.setToy(toy);
                ol.setToyID(rs2.getInt("toyID"));

                order.addOrderlines(ol);
            }
            rs2.close();
            
            all.add(order);
        }

        rs.close();
        conn.close();

        return all;
    }

    public Order setOrderData(ResultSet rs) throws SQLException {
        Order order = new Order();
        order.setAddr(rs.getString("deliveryAddr"));
        order.setOrderBy(rs.getInt("orderBy"));
        order.setOrderID(rs.getInt("orderID"));
        order.setOrderDate(rs.getDate("orderDate"));

        UserService userSrv = new UserService(this.servlet);
        order.setOrderByUser(userSrv.queryById(rs.getString("orderBy")));

        return order;
    }
    
    public Map<String, Integer> getBrandReport() throws SQLException{
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        
        Connection conn = JDBCUtil.getConnection(this.servlet);
        ResultSet rs = JDBCUtil.query(conn, "SELECT brand, count(*) as count FROM dbo.orderline, dbo.toy" +
            " where orderline.toyID = toy.toyID" +
            " group by brand");
        
        while(rs.next()){
            map.put(rs.getString("brand"), rs.getInt("count"));
        }
        
        rs.close();
        conn.close();

        return map;
    }
    
    public double getTotalSaleAmountByMonth(int year, int month) throws SQLException{
        double amount = 0;
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        
        Connection conn = JDBCUtil.getConnection(this.servlet);
        String sql = "select sum(subtotal) from dbo.orderline, dbo.[order]" +
            " where orderline.orderID = [order].orderID\n" +
            " and DATEPART(yy, orderDate) = " + year + 
            " AND DATEPART(mm, orderDate) = " + month;
        
        
        ResultSet rs = JDBCUtil.query(conn, sql);
        
        if(rs.first())
            amount = rs.getDouble(1);

        rs.close();
        conn.close();
        return amount;
    }

}
