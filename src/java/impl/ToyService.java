/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package impl;

import bean.SecondHandToy;
import bean.Toy;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;
import javax.servlet.http.HttpServlet;
import service.DbService;
import util.JDBCUtil;

/**
 *
 * @author Fai
 */
public class ToyService extends DbService<Toy> {
    public ToyService(HttpServlet servlet){
        super(servlet);
    }    

    @Override
    public Toy insert(Toy t) throws SQLException {
        Connection conn = JDBCUtil.getConnection(servlet);

               
        String query = "INSERT INTO [dbo].[toy](header,description,sex,age,brand,price,image,isDeleted) VALUES (?, ?, ?, ?, ?,?,?,?)";
        PreparedStatement pstmt = conn.prepareStatement(query);
        pstmt.setString(1,t.getHeader());
        pstmt.setString(2,t.getDescription());
        pstmt.setString(3,t.getSex());
        pstmt.setString(4,t.getAge());
        pstmt.setString(5,t.getBrand());
        pstmt.setDouble(6,t.getPrice());
        pstmt.setString(7,t.getImage());
        pstmt.setBoolean(8,t.isDeleted());
        int rows = pstmt.executeUpdate();
        if (rows > 0) {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT @@IDENTITY AS [@@IDENTITY]");
            if (rs != null && rs.next() != false) {
                t.setToyID(rs.getInt(1));
                rs.close();
            }
            return t;
        } else {
            
            return null;
        }
         
    }

    @Override
    public int update(Toy t) throws SQLException {
        Connection conn = JDBCUtil.getConnection(servlet);
        
        if(t == null)
            return 0;
        
        
        String query = "UPDATE [dbo].[toy] SET header = ? , description = ?, sex = ?, age = ?,brand =?, price = ?, image = ? WHERE toyID = ?";
        PreparedStatement pstmt = conn.prepareStatement(query);
        pstmt.setString(1,t.getHeader());
        pstmt.setString(2,t.getDescription());
        pstmt.setString(3,t.getSex());
        pstmt.setString(4,t.getAge());
        pstmt.setString(5,t.getBrand());
        pstmt.setDouble(6,t.getPrice());
        pstmt.setString(7,t.getImage());
        pstmt.setInt(8,t.getToyID());
        int rows = pstmt.executeUpdate();
        return rows;
    }

    @Override
    public int delete(Toy t) throws SQLException {
        Connection conn = JDBCUtil.getConnection(servlet);
        if(t == null)
            return 0;
        
        String query = "UPDATE [dbo].[toy] SET isDeleted = 1 WHERE toyID = ?";
        PreparedStatement pstmt = conn.prepareStatement(query);
        pstmt.setInt(1,t.getToyID());
        int rows = pstmt.executeUpdate();
        return rows;
    }

    @Override
    public Toy query(Toy t) throws SQLException {
        Connection conn = JDBCUtil.getConnection(servlet);
        ResultSet rs = JDBCUtil.query(conn, "select * from dbo.toy where toyID = '" + t.getToyID() + "'");
        
        rs.first();
        
        Toy toy = setToyData(rs);
        
        rs.close();
        conn.close();
        
        return toy;     
    }

    @Override
    public Vector<Toy> all() throws SQLException {     
        Vector<Toy> toys = new Vector<Toy>();
        
        Connection conn = JDBCUtil.getConnection(servlet);
        ResultSet rs = JDBCUtil.query(conn, "select * from dbo.toy where isDeleted = 0 and  toyID not in(select toyID from dbo.secondhandtoy where isSold = 1)");
        
        while(rs.next()){
            Toy toy = setToyData(rs);
            toys.add(toy);
        }
        
        rs.close();
        conn.close();
        
        return toys;
    }

     public Vector<Toy> allNonSecondHand(String brand, String gender, String age, String price) throws SQLException {     
        Vector<Toy> toys = new Vector<Toy>();
        String brandRequest = "";
        String genderRequest = "";
        if(brand!=null) {
            brandRequest = " and UPPER(brand) = UPPER('"+ brand +"')";
        }
        if(gender!=null) {
            genderRequest = " and UPPER(sex) LIKE '%" + gender.toUpperCase() + "%'";
        }
        String ageRequest = "";
        if(age!=null) {
            if(age.equals("0")){
                //0-3
                ageRequest = " and age IN ('0','1','2','3')";
            }else if(age.equals("4")){
                //4-7
                ageRequest = " and age IN ('4','5','6','7')";
            }else if(age.equals("8")){
                //8-11
                ageRequest = " and age IN ('8','9','10','11')";
            }else if(age.equals("12")){
                //12-15
                ageRequest = " and age IN ('12','13','14','15')";
            }else if(age.equals("16")){
                //16-18
                ageRequest = " and age IN ('16','17','18')";
            }
        }
        String priceRequest = "";
        if(price!=null) {
            if(price.equals("0")){
                //below 50
                priceRequest = " and price < 50";
            }else if(price.equals("50")){
                //50 - 99
                priceRequest = " and price between 50 and 99.99";
            }else if(price.equals("100")){
                //100 - 199
                priceRequest = " and price between 100 and 199.99";
            }else if(price.equals("200")){
                //200 - 499
                priceRequest = " and price between 200 and 499.99";
            }else if(price.equals("500")){
                //500 above
                priceRequest = " and price >= 500";
            }
        }
        Connection conn = JDBCUtil.getConnection(servlet);
        ResultSet rs = JDBCUtil.query(conn, "select * from dbo.toy where toyID not in(select toyID from dbo.secondhandtoy) and isDeleted = 0" + brandRequest + genderRequest + ageRequest + priceRequest);
        
        while(rs.next()){
            Toy toy = setToyData(rs);
            toys.add(toy);
        }
        
        rs.close();
        conn.close();
        
        return toys;
    }
      public Vector<String> getBrand() throws SQLException {     
        Vector<String> s = new Vector<String>();
        
        Connection conn = JDBCUtil.getConnection(servlet);
        ResultSet rs = JDBCUtil.query(conn, "select distinct brand from dbo.toy where toyID not in(select toyID from dbo.secondhandtoy where approved=0 or isSold = 1) and isDeleted = 0");
        
        while(rs.next()){
            String brand = rs.getString("brand");
            s.add(brand);
        }
        
        rs.close();
        conn.close();
        
        return s;
    }
      public Vector<Toy> allForSold(String brand, String gender, String age, String price) throws SQLException {     
        Vector<Toy> toys = new Vector<Toy>();
        String brandRequest = "";
        if(brand!=null) {
            brandRequest = " and UPPER(brand) = UPPER('"+ brand +"')";
        }
        String genderRequest = "";
        if(gender!=null) {
            genderRequest = " and UPPER(sex) LIKE '%" + gender.toUpperCase() + "%'";
        }
        String ageRequest = "";
        if(age!=null) {
            if(age.equals("0")){
                //0-3
                ageRequest = " and age IN ('0','1','2','3')";
            }else if(age.equals("4")){
                //4-7
                ageRequest = " and age IN ('4','5','6','7')";
            }else if(age.equals("8")){
                //8-11
                ageRequest = " and age IN ('8','9','10','11')";
            }else if(age.equals("12")){
                //12-15
                ageRequest = " and age IN ('12','13','14','15')";
            }else if(age.equals("16")){
                //16-18
                ageRequest = " and age IN ('16','17','18')";
            }
        }
        String priceRequest = "";
        if(price!=null) {
            if(price.equals("0")){
                //below 50
                priceRequest = " and price < 50";
            }else if(price.equals("50")){
                //50 - 99
                priceRequest = " and price between 50 and 99.99";
            }else if(price.equals("100")){
                //100 - 199
                priceRequest = " and price between 100 and 199.99";
            }else if(price.equals("200")){
                //200 - 499
                priceRequest = " and price between 200 and 499.99";
            }else if(price.equals("500")){
                //500 above
                priceRequest = " and price >= 500";
            }
        }
        Connection conn = JDBCUtil.getConnection(servlet);
        ResultSet rs = JDBCUtil.query(conn, "select * from dbo.toy where toyID not in(select toyID from dbo.secondhandtoy where approved=0 or isSold=1) and isDeleted = 0" + brandRequest + genderRequest + ageRequest + priceRequest );
        
        while(rs.next()){
            Toy toy = setToyData(rs);
            toys.add(toy);
        }
        
        rs.close();
        conn.close();
        
        return toys;
    }
    public Vector<Toy> allApprovedSecondHand(String brand, String gender, String age, String price) throws SQLException {     
        Vector<Toy> toys = new Vector<Toy>();
        String brandRequest = "";
        if(brand!=null) {
            brandRequest = " and UPPER(brand) = UPPER('"+ brand +"')";
        }
        String genderRequest = "";
        if(gender!=null) {
            genderRequest = " and UPPER(sex) LIKE '%" + gender.toUpperCase() + "%'";
        }
        String ageRequest = "";
        if(age!=null) {
            if(age.equals("0")){
                //0-3
                ageRequest = " and age IN ('0','1','2','3')";
            }else if(age.equals("4")){
                //4-7
                ageRequest = " and age IN ('4','5','6','7')";
            }else if(age.equals("8")){
                //8-11
                ageRequest = " and age IN ('8','9','10','11')";
            }else if(age.equals("12")){
                //12-15
                ageRequest = " and age IN ('12','13','14','15')";
            }else if(age.equals("16")){
                //16-18
                ageRequest = " and age IN ('16','17','18')";
            }
        }
        String priceRequest = "";
        if(price!=null) {
            if(price.equals("0")){
                //below 50
                priceRequest = " and price < 50";
            }else if(price.equals("50")){
                //50 - 99
                priceRequest = " and price between 50 and 99.99";
            }else if(price.equals("100")){
                //100 - 199
                priceRequest = " and price between 100 and 199.99";
            }else if(price.equals("200")){
                //200 - 499
                priceRequest = " and price between 200 and 499.99";
            }else if(price.equals("500")){
                //500 above
                priceRequest = " and price >= 500";
            }
        }
        Connection conn = JDBCUtil.getConnection(servlet);
        ResultSet rs = JDBCUtil.query(conn, "select * from dbo.toy where toyID in(select toyID from dbo.secondhandtoy where approved=1 and isSold=0) and isDeleted = 0" + brandRequest + genderRequest + ageRequest + priceRequest);
        
        while(rs.next()){
            Toy toy = setToyData(rs);
            toys.add(toy);
        }
        
        rs.close();
        conn.close();
        
        return toys;
    }
      public Vector<Toy> allUnapprovedSecondHand() throws SQLException {     
        Vector<Toy> toys = new Vector<Toy>();
        
        Connection conn = JDBCUtil.getConnection(servlet);
        ResultSet rs = JDBCUtil.query(conn, "select * from dbo.toy where toyID in(select toyID from dbo.secondhandtoy where approved=0) and isDeleted = 0");
        
        while(rs.next()){
            Toy toy = setToyData(rs);
            toys.add(toy);
        }
        
        rs.close();
        conn.close();
        
        return toys;
    }
    
    public Toy setToyData(ResultSet rs) throws SQLException{
        Toy toy = new Toy();
        toy.setToyID(rs.getInt("toyID"));
        toy.setHeader(rs.getString("header"));
        toy.setDescription(rs.getString("description"));
        toy.setSex(rs.getString("sex"));
        toy.setAge(rs.getString("age"));
        toy.setBrand(rs.getString("brand"));
        toy.setPrice(rs.getDouble("price"));
        toy.setImage(rs.getString("image"));
        SecondHandToyService shtSrv = new SecondHandToyService(this.servlet);
        SecondHandToy sToy = new SecondHandToy();
        //sToy.setToy(toy);
        sToy.setToyID(toy.getToyID());
        
        sToy = shtSrv.query(sToy);
        toy.setSecondHandToy(sToy);
        
        return toy;
    }
   
}
