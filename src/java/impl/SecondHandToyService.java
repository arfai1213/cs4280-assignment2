/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package impl;

import bean.SecondHandToy;
import bean.Toy;
import bean.User;
import static java.rmi.server.LogStream.log;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Vector;
import static javax.print.attribute.Size2DSyntax.MM;
import javax.servlet.http.HttpServlet;
import service.DbService;
import util.JDBCUtil;

/**
 *
 * @author Fai
 */
public class SecondHandToyService extends DbService<SecondHandToy>{
    public SecondHandToyService(HttpServlet servlet) {
        super(servlet);
    }

    @Override
    public SecondHandToy insert(SecondHandToy t) throws SQLException {
        Connection conn = JDBCUtil.getConnection(servlet);

        
        String query = "INSERT INTO [dbo].[secondhandtoy](toyID,approved,createDate,soldBy, isSold) VALUES (?,?,convert(datetime,?),?, 0)";
        PreparedStatement pstmt = conn.prepareStatement(query);
        pstmt.setInt(1,t.getToyID());
        pstmt.setBoolean(2,t.isApproved());
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String msSqlDate=sdf.format(t.getCreateDate()).trim();
        pstmt.setString(3,msSqlDate);
        pstmt.setInt(4,t.getSoldByCust());
        int rows = pstmt.executeUpdate();
        if (rows > 0) {
            return t;
        } else {
            return null;
        }
         
    }

    @Override
    public int update(SecondHandToy t) throws SQLException {
        Connection conn = JDBCUtil.getConnection(servlet);
        
        if(t == null)
            return 0;
        
        
        String query = "UPDATE [dbo].[secondhandtoy] SET approved = ? WHERE toyID = ?";
        PreparedStatement pstmt = conn.prepareStatement(query);
        pstmt.setBoolean(1,t.isApproved());
        pstmt.setInt(2,t.getToyID());
        int rows = pstmt.executeUpdate();
        return rows;
    }

    @Override
    public int delete(SecondHandToy t) throws SQLException {
        Connection conn = JDBCUtil.getConnection(servlet);
        if(t == null)
            return 0;
        
        //String query = "DELETE FROM [dbo].[secondhandtoy] WHERE toyID = ?";
        String query2 = "UPDATE [dbo].[toy] SET isDeleted = 1 WHERE toyID = ?";
        //PreparedStatement pstmt = conn.prepareStatement(query);
        PreparedStatement pstmt2 = conn.prepareStatement(query2);
        //pstmt.setInt(1,t.getToyID());
        pstmt2.setInt(1,t.getToyID());
        
        //int rows = pstmt.executeUpdate();
        int rows = pstmt2.executeUpdate();
        return rows;
    }

    @Override
    public SecondHandToy query(SecondHandToy t) throws SQLException {
        Connection conn = JDBCUtil.getConnection(servlet);
        ResultSet rs = JDBCUtil.query(conn, "select * from dbo.secondhandtoy where toyID = '" + t.getToyID()+ "'");
        
        rs.first();
        
        SecondHandToy secondtoy = setSecondHandToyData(rs);
        
        rs.close();
        conn.close();
        
        return secondtoy;     
    }

    @Override
    public Vector<SecondHandToy> all() throws SQLException {
        Vector<SecondHandToy> toys = new Vector<SecondHandToy>();
        
        Connection conn = JDBCUtil.getConnection(servlet);
        ResultSet rs = JDBCUtil.query(conn, "select * from dbo.secondhandtoy");
        
        while(rs.next()){
            SecondHandToy toy = setSecondHandToyData(rs);
            ToyService toyService = new ToyService(servlet);
            Toy toyTemp = new Toy();
            toyTemp.setToyID(toy.getToyID());
            toyTemp = toyService.query(toyTemp);
            toy.setToy(toyTemp);
            toys.add(toy);
        }
        
        rs.close();
        conn.close();
        
        return toys;
    }
    public SecondHandToy setSecondHandToyData(ResultSet rs) throws SQLException{
        if(rs.getRow() == 0)
            return null;
        
        SecondHandToy toy = new SecondHandToy();
        
        toy.setApproved(rs.getBoolean("approved"));
        toy.setCreateDate(rs.getDate("createDate"));
        toy.setToyID(rs.getInt("ToyID"));
        toy.setSoldByCust(rs.getInt("soldBy"));
        toy.setIsSold(rs.getBoolean("isSold"));
        //ToyService toySv = new ToyService(this.servlet);
        //Toy tmp = new Toy();
        //tmp.setToyID(rs.getInt("ToyID"));
        //tmp = toySv.query(tmp);
        
        //toy.setToy(tmp);
        if(toy.getSoldBy() == null){
            UserService usrSrv = new UserService(this.servlet);
            
            User user = usrSrv.queryById(""+rs.getInt("soldBy"));
            toy.setSoldBy(user);
        }
        
        return toy;
    }
    
    public Vector<SecondHandToy> queryByUserId(User user) throws SQLException {
        Vector<SecondHandToy> toys = new Vector<SecondHandToy>();
        
        Connection conn = JDBCUtil.getConnection(servlet);
        ResultSet rs = JDBCUtil.query(conn, "select * from dbo.secondhandtoy where soldBy = " + user.getUserID());
        
        while(rs.next()){
            SecondHandToy toy = setSecondHandToyData(rs);
            ToyService toyService = new ToyService(servlet);
            Toy toyTemp = new Toy();
            toyTemp.setToyID(toy.getToyID());
            toyTemp = toyService.query(toyTemp);
            toy.setToy(toyTemp);
            toys.add(toy);
        }
        
        rs.close();
        conn.close();
        
        return toys;
    }
    
}
