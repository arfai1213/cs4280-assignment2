/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package impl;

import bean.Comment;
import bean.Toy;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Vector;
import javax.servlet.http.HttpServlet;
import service.DbService;
import util.JDBCUtil;

/**
 *
 * @author Fai
 */
public class CommentService extends DbService<Comment>{

    public CommentService(HttpServlet servlet) {
        super(servlet);
    }

    @Override
    public Comment insert(Comment t) throws SQLException {
        Connection conn = JDBCUtil.getConnection(servlet);
        PreparedStatement pStmnt = conn.prepareStatement("insert into dbo.comment (content, toyID, replyTo, date, userID) values (?,?,?,getdate(),?);");
        pStmnt.setString(1, t.getContent());
        pStmnt.setInt(2, t.getToyID());
        
        if(t.getReplyTo() == null){
            pStmnt.setNull(3, Types.INTEGER);
        }else{
            pStmnt.setInt(3, t.getReplyTo());
        }
        
        
        if(t.getUserId()== null){
            pStmnt.setNull(4, Types.INTEGER);
        }else{
            pStmnt.setInt(4, t.getUserId());
        }
        
        int row = pStmnt.executeUpdate();
        
        if(row == 1)
            return t;
        else 
            return null;
    }

    @Override
    public int update(Comment t) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int delete(Comment t) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Comment query(Comment t) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public Vector<Comment> getByToyID(Toy toy) throws SQLException{
        Connection conn = JDBCUtil.getConnection(servlet);
        String sql = "select c.*, displayName from dbo.comment c left join [dbo].[user] u on c.userId = u.userId"
                + " where toyID = '" +  toy.getToyID() +"' and replyTo is null" ;
        
        ResultSet rs = JDBCUtil.query(conn, sql);
        Vector<Comment> comments = new Vector<Comment>();
        
        while(rs.next()){
            Comment comment = new Comment();
            comment.setCommentID(rs.getInt("commentID"));
            comment.setContent(rs.getString("content"));
            comment.setDate(rs.getTimestamp("date"));
            comment.setReplyTo(rs.getInt("replyTo"));
            comment.setToyID(rs.getInt("toyID"));
            
            comment.setUserId(rs.getInt("userID"));
            comment.setReplies(this.getReplies(comment));
            
            UserService userSrv = new UserService(this.servlet);
            comment.setReplyToUser(userSrv.queryById(comment.getUserId() + ""));

            comments.add(comment);
        }
         
        rs.close();
        conn.close();
        
        return comments;
    }

    public Vector<Comment> getReplies(Comment cmt) throws SQLException{
        Connection conn = JDBCUtil.getConnection(servlet);
        String sql = "select c.*, displayName from dbo.comment c left join [dbo].[user] u on c.userId = u.userId"
                + " where replyTo = '" +  cmt.getCommentID() +"' " ;
        
        ResultSet rs = JDBCUtil.query(conn, sql);
        Vector<Comment> comments = new Vector<Comment>();
                
        while(rs.next()){
            Comment comment = new Comment();
            comment.setCommentID(rs.getInt("commentID"));
            comment.setContent(rs.getString("content"));
            comment.setDate(rs.getTimestamp("date"));
            comment.setReplyTo(rs.getInt("replyTo"));
            comment.setToyID(rs.getInt("toyID"));
            
            comment.setUserId(rs.getInt("userID"));
            UserService userSrv = new UserService(this.servlet);
            comment.setReplyToUser(userSrv.queryById(comment.getUserId() + ""));
            
            comments.add(comment);
        }
         
        rs.close();
        conn.close();
        
        return comments;
    }

    
    @Override
    public Vector<Comment> all() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
