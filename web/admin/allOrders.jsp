<%@page import="java.util.Map"%>
<%@page import="bean.Orderline"%>
<%@page import="bean.Order"%>
<%@page import="bean.User"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="bean.Comment"%>
<%@page import="java.util.Vector"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="bean.Toy"%>
<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Foundation | Welcome</title>
        <link rel="stylesheet" href="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.min.css">
        <link href='http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/motion-ui/1.1.1/motion-ui.min.css" />
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css" />
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/dataTables.foundation.min.css" />

    </head>
    <body>
        <jsp:include page="../include/header.jsp"/>

        <br>

        <div class="row columns">
            <nav aria-label="You are here:" role="navigation">
                <ul class="breadcrumbs">
                    <li><a href="home">Home</a></li>
                    <li>
                        <span class="show-for-sr">Current: </span> ALL ORDER
                    </li>
                </ul>
            </nav>
        </div>

        <div class="row columns">
            <div class="columns small-12 large-5">
                
                <div>
                    <div>
                        <%
                            Double totalAmount = (Double) request.getAttribute("totalSales");
                        %>
                        <div>Total sales amount in this month:</div>
                        <center><h3>$<%= totalAmount %></h3></center>
                    </div>
                </div>
                </br>
                <%
                    Map<String, Integer> data = (Map<String, Integer>) request.getAttribute("brandReportData");

                    //Convert map to javascript map
                    String arr = "[";
                    for (Map.Entry<String, Integer> entry : data.entrySet()) {
                        arr += ",['"+entry.getKey() + "', " + entry.getValue() + "]";
                    }
                    arr+= "]";
                    arr = arr.replaceFirst(",", "");
                %>
                <div id="report1" style="height: 400px"></div>
            </div>

            <div class="columns small-12 large-7">
                <table id="orders" class="display">
                    <thead>
                    <th>Order ID</th>
                    <th>Order Date</th>
                    <th>Order by User</th>
                    <th>Total purchased toys</th>
                    <th>Total amount</th>
                    <th></th>
                    </thead>
                    <tbody>
                        <%
                            Vector<Order> allOrders = (Vector<Order>) request.getAttribute("allOrders");
                            if (allOrders != null) {
                                for (int i = 0; i < allOrders.size(); i++) {
                        %>
                        <%
                            Order order = allOrders.get(i);
                        %>
                        <tr>
                            <td><%= order.getOrderID()%></td>
                            <td><%= order.getOrderDate()%></td>
                            <td><%= order.getOrderByUser().getDisplayName()%></td>
                            <td><%= order.getNumberOfToysPurchased()%></td>
                            <td>$<%= order.getTotal()%></td>
                            <td><a class="button" href="order?id=<%= order.getOrderID()%>">View</a></td>
                        </tr>
                        <%      }

                            }%>
                    </tbody>
                </table>
            </div>
        </div>


        <div class="row columns">
            <jsp:include page="../include/footer.html"/>
        </div>

        <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
        <script src="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>
        <script type="text/javascript" src="https://intercom.zurb.com/scripts/zcom.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.11/js/dataTables.foundation.min.js"></script>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/highcharts-3d.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>


        <script>
            $(document).foundation();
            $(document).ready(function () {
                $('#orders').DataTable({
                    "dom": 'frtip'
                });

                $('#report1').highcharts({
                    chart: {
                        type: 'pie',
                        options3d: {
                            enabled: true,
                            alpha: 45
                        }
                    },
                    title: {
                        text: 'Sales report by toy \'s brand'
                    },
                    plotOptions: {
                        pie: {
                            innerSize: 100,
                            depth: 45
                        }
                    },
                    series: [{
                            name: 'Purchased amount',
                            data: <%= arr %>
                        }]
                });

            });
        </script>

    </body>
</html>
