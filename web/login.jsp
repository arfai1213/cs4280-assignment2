<%-- 
    Document   : login
    Created on : Mar 27, 2016, 5:46:21 PM
    Author     : Fai
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Toys4Kids | Login</title>
    </head>

    <link rel="stylesheet" href="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.min.css">
    <link href='http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>
    <style>
        body {
            background: #F0F0F3;
        }
        .login-box {
            background: #fff;
            border: 1px solid #ddd; 
            margin: 100px 0;
            padding: 40px 20px 0 20px;
        }
    </style>

    <body>
        <div class="large-3 large-centered columns">
            <div class="login-box">
                <div>
                    <img class="thumbnail" src="images/logo.png" alt="Photo of Uranus.">
                </div>

                <div class="row">
                    <div class="large-12 columns">
                        <form method="post" action="user" >
                            <input type="hidden" value="login" name="action"/>

                            <div class="input-group">
                                <span class="input-group-label"><i class="fi-torso"></i></span>
                                <input class="input-group-field" type="text" placeholder="Username" name="username"/>
                            </div>
                            <div class="input-group">
                                <span class="input-group-label"><i class="fi-lock"></i></span>
                                <input class="input-group-field" type="password" placeholder="password" name="password"/>
                            </div>

                            <div class="input-group">
                                <%
                                    String msg = (String) session.getAttribute("msg");
                                    if (msg != null) {
                                        session.removeAttribute("msg");
                                        out.println(msg);
                                    }
                                %>
                            </div>

                            <div class="input-group">
                                <input type="submit" class="button expand float-right" value="Log In"/>
                                <a class="button expand float-right" href="register.jsp">Sign up</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <jsp:include page="include/footer.html"/>
    </body>
</html>
