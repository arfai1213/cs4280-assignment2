<%@page import="bean.User"%>
<%@page import="util.SessionChecker"%>
<div class="top-bar">
    <div class="row">
        <div class="top-bar-left">
            <ul class="dropdown menu" data-dropdown-menu>
                <li class="menu-text" onclick="location.href = '.'" style="cursor: pointer">TOYS4KIDS Online</li>
                <li class="has-submenu">
                    <a href="#">Toys</a>
                    <ul class="submenu menu vertical" data-submenu>
                        <li><a href="toys">Browse all toys</a></li>
                        <li><a href="first-hand-toys">Browse new toys</a></li>
                        <li><a href="2nd-hand-toys">Browse recycled toy</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="top-bar-right">
            <ul class="dropdown menu" data-dropdown-menu>
                <%
                    User user = SessionChecker.getCurrentUser(session);
                %>
                
                <% 
                    if(user != null && user.isCustomer()){
                        //admin have no access to cart
                %>
                <li>
                    <a href="cart">
                        <i class="fi-shopping-cart">&nbsp;</i>Cart
                    </a>
                </li>
                <%
                    }
                    if (user == null) {
                %>
                <li>
                    <a href="login.jsp"><i class="fi-torso"></i>Login</a>
                </li>
                <%
                    }else if(user.isCustomer()){
                %>
                <li class="has-submenu">
                    <a href="#"><i class="fi-torso"></i>&nbsp;<%= user.getDisplayName()  %></a>
                    <ul class="submenu menu vertical" data-submenu>
                        <li><a href="create-2nd-hand-toy">Recycle toys</a></li>
                        <li><a href="SecondHandToy">Recycle toy status</a></li>
                        <li><a href="order">View order</a></li>
                        <hr/>
                        <li><a href="#">Credit : <%= user.getCredit() %></a></li>
                        <li><a href="./user?action=logout">Logout</a></li>
                    </ul>
                </li>
                <% }else{ %>
                <li class="has-submenu">
                    <a href="#"><i class="fi-torso"></i>&nbsp;<%= user.getDisplayName() %></a>
                    <ul class="submenu menu vertical" data-submenu>
                        <li><a href="approve-2nd-hand-toys">Approve recycle toy</a></li>
                        <li><a href="maintain-toys">Maintain toy</a></li>
                        <li><a href="order">View order</a></li>
                        <hr/>
                        <li><a href="./user?action=logout">Logout</a></li>
                    </ul>
                </li>
                <% } %>
            </ul>
        </div>
    </div>
</div>