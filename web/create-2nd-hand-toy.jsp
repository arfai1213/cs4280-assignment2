<%@page import="bean.User"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="bean.Comment"%>
<%@page import="java.util.Vector"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="bean.Toy"%>
<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Toys4Kids | Create Recycled toy</title>
        <link href="style/foundation.css" rel="stylesheet">

        <link rel="stylesheet" href="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.min.css">
        <link href='http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>
        <link href="style/main.css" rel="stylesheet">

    </head>
    <body>
        <div>
            <jsp:include page="include/header.jsp"/>
                    <br>
                            <div class="row columns">
                        <nav aria-label="You are here:" role="navigation">
                <ul class="breadcrumbs">
                    <li><a href="home">Home</a></li>
                    
                    <li>
                        <span class="show-for-sr">Current: </span> Create RECYCLED Toy
                    </li>
                </ul>
            </nav>
        </div>
        <hr>
            <form action="create-2nd-hand-toy" method="POST" enctype="multipart/form-data">
                                <div class="row">
                    <div class="large-3 columns" style="text-align: center; width: 50%; padding: 30px;">
                        <div class="thumbnail item-image-container" >
                          <div class="row collapse prefix-radius">
                          <div class="large-centered columns">
                              <input type="file" name="file" id="file" style="width: auto; background-color: rgba(255,255,255,0.9);"/>
                              <input type="hidden" name="image" id="image"/>
                          </div>
                            </div>
                            <div class="row collapse prefix-radius">
                          <div class="large-centered columns">
                              <a class="primary button" id="upload">Upload</a>
                          </div>
                            </div>
                        </div>
                    </div>
                    <div class="large-6 columns">
                        <div class="row">
                          <div class="large-12 columns">
                            <label>Toy Name
                                <input type="text" name="header" placeholder="Toy Header" required/>

                            </label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="large-12 columns">
                            <label>Description
                                <textarea name="description" required ></textarea>
                            </label>
                          </div>
                        </div>
                        <div class="row">
                            <div class="large-6 columns">
                              <div class="row collapse prefix-radius">
                                <div class="small-3 columns">
                                  <span class="prefix">Sex</span>
                                </div>
                                <div class="small-9 columns">
                                    <input type="checkbox" name="sex" value="M" ><label for="sexM">Male</label>
                                    <input type="checkbox" name="sex" value="F" ><label for="sexF">Female</label>
                                </div>
                              </div>
                                                           </div>
                            <div class="large-6 columns">
                              <div class="row collapse prefix-radius">
                                <div class="small-3 columns">
                                  <span class="prefix">Age</span>
                                </div>
                                <div class="small-9 columns">
                                    <input type="number" name="age" placeholder="Age" required/>
                                </div>
                              </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-6 columns">
                              <div class="row collapse prefix-radius">
                                <div class="small-3 columns">
                                  <span class="prefix">Brand</span>
                                </div>
                                <div class="small-9 columns">
                                    <input type="text" name="brand" placeholder="Brand" required/>
                                </div>
                              </div>
                            </div>
                            <div class="large-6 columns">
                              <div class="row collapse prefix-radius">
                                <div class="small-3 columns">
                                  <span class="prefix">Price</span>
                                </div>
                                <div class="small-9 columns">
                                    <input type="number" name="price" placeholder="Price" value="" required/>
                                </div>
                              </div>
                            </div>
                        </div>  
                        <div class="row">
                            <div class="large-12 columns">
                                <input class="primary button"  style="width: 100%;" type="submit" value="Save" />
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            
            
        <div class="row column">
            <hr>            
            <jsp:include page="include/footer.html"/>
        </div>
        <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
        <script src="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>

        <script>
            $(document).foundation();
            $('#upload').on('click',function(){
                $(this).html('Uploading').attr('disabled','disabled');
                var formData = new FormData();
                formData.append('upload', $('#file')[0].files[0]);
                $.ajax({
                    url: 'http://uploads.im/api',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(data){
                       $('#upload').html('Upload').removeAttr('disabled');
                       $('#image').val(data.data['thumb_url']);
                       $('.item-image-container').css({'background-image': 'url('+data.data['thumb_url']+')'});
                    }
                });
                
            });
            <%
                String msg = (String) session.getAttribute("msg");
                session.removeAttribute("msg");
            %>
            var msg = "<%= msg%>"

            if (msg != "null")
                alert(msg);
            
            $(document).ready(function(){
                $('.reply').click(function(){
                    var parentDiv = $(this).parent().parent();
                    $('.reply-form').hide();
                    parentDiv.find('.reply-form').show();
                    return false;
                });
                
                $('.showReply').click(function(){
                    var parentDiv = $(this).parent().parent();
                    parentDiv.find('.replies').show();
                    return false;
                });
                
                $('#addCart').click(function(){
                    var _href = $(this).attr("href"); 
                    $(this).attr("href", _href + '&qty=' + $('#qty').val());
                    return true;
                });
            })

        </script>
        <script type="text/javascript" src="https://intercom.zurb.com/scripts/zcom.js"></script>
        
    </body>
</html>
