<%@page import="bean.User"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="bean.Comment"%>
<%@page import="java.util.Vector"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="bean.Toy"%>
<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Toys4Kids | Recycled toys approval</title>
        <link rel="stylesheet" href="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.min.css">
        <link href='http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>

    </head>
    <body>
        <jsp:include page="include/header.jsp"/>
        
        <br>

        <%
            Vector<Toy> toys = (Vector<Toy>) request.getAttribute("toys");
            String category = (String) request.getAttribute("category");
            session = request.getSession();
        %>
        
        <div class="row columns">
            <nav aria-label="You are here:" role="navigation">
                <ul class="breadcrumbs" style="display:inline">
                    <li><a href="home">Home</a></li>
                    <li>
                        <span class="show-for-sr">Current: </span> <%=category %>
                    </li>
                </ul>
                    <span class="float-right" style="display: inline"><a href="SecondHandToy"><i class="fi-list-thumbnails"></i>&nbsp;Table view</a></span>        
            </nav>
        </div>
                    
       <div class="row">
        <% for(Toy toy : toys ) { %>
            <div class="" style="width: 350px;padding:30px; float:left;">
            <div class="row">
                <img class="thumbnail" src="<%=toy.getImage()%>">
            </div>
            <div class="row">
                <h3><%= toy.getHeader()%></h3>
            </div>
            <div class="row">
                <%if(toy.getSecondHandToy()!=null ){%>
                <div class="small-12 columns">
                    <label class="middle">Second Hand Toy</label>
                </div>
                <div class="small-3 columns">
                    <label class="middle">Seller</label>
                </div>
                <div class="small-9 columns">
                    <label class="middle"><%=toy.getSecondHandToy().getSoldBy().getDisplayName() %></label>
                </div>
                <% } %>
                <div class="small-3 columns">
                    <label class="middle">Brand</label>
                </div>
                <div class="small-9 columns">
                    <label class="middle"><%= toy.getBrand()%></label>
                </div>
            </div>
            <div class="row">

                <div class="small-3 columns">
                    <label class="middle">Price</label>
                </div>
                <div class="small-9 columns">
                    <%
                        DecimalFormat df = new DecimalFormat("0.00");
                    %>
                    <label class="middle"><%= "$" + df.format(toy.getPrice())%></label>
                </div>
            </div>
            <div class="row">
                <a id="addCart" href="approve-2nd-hand-toys?action=approve&id=<%= toy.getToyID() %>" class="primary button">Approve</a>
            </div>
            </div>       
        <% }  %>
        </div>
        <div class="row column">
            <hr>
            <jsp:include page="include/footer.html"/>
        </div>
        <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
        <script src="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>

        <script>
            $(document).foundation();

            <%
                String msg = (String) session.getAttribute("msg");
                session.removeAttribute("msg");
            %>
            var msg = "<%= msg%>"

            if (msg != "null")
                alert(msg);
            
            $(document).ready(function(){
                $('.reply').click(function(){
                    var parentDiv = $(this).parent().parent();
                    $('.reply-form').hide();
                    parentDiv.find('.reply-form').show();
                    return false;
                });
                
                $('.showReply').click(function(){
                    var parentDiv = $(this).parent().parent();
                    parentDiv.find('.replies').show();
                    return false;
                });
                
                $('#addCart').click(function(){
                    var _href = $(this).attr("href"); 
                    $(this).attr("href", _href + '&qty=' + $('#qty').val());
                    return true;
                });
            })

        </script>
        <script type="text/javascript" src="https://intercom.zurb.com/scripts/zcom.js"></script>
        
    </body>
</html>
