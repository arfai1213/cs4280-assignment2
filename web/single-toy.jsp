<%@page import="bean.SecondHandToy"%>
<%@page import="bean.User"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="bean.Comment"%>
<%@page import="java.util.Vector"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="bean.Toy"%>
<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Toys4Kids | Toy's details</title>
        <link rel="stylesheet" href="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.min.css">
        <link href='http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="./style/main.css">
    </head>
    <body>
        <jsp:include page="include/header.jsp"/>
        
        <br>

        <%
            Toy toy = (Toy) request.getAttribute("toy");
            session = request.getSession();
            
            String className = "";
            if(toy.isSecondHand()){
                className = "second-hand-label";
            }
        %>
        
        <div class="row columns">
            <nav aria-label="You are here:" role="navigation">
                <ul class="breadcrumbs">
                    <li><a href="home">Home</a></li>
                    <li>
                        <span class="show-for-sr">Current: </span> TOY
                    </li>
                </ul>
            </nav>
        </div>
        <div class="row">
            <div class="medium-6 columns">
                <div class="item-image-container thumbnail <%= className %>" style="background-image: url('<%=toy.getImage() %>');">
                </div>
            </div>
            <div class="medium-6 large-6 columns">
                <h3>
                    <%= toy.getHeader()%>
                </h3>
                <p>
                    <%
                        if(toy.getSecondHandToy() != null){
                    %>
                        <div><span class="secondary label">Second hand toy</span></div>
                        </br>
                    <% } %>
                    <%= toy.getDescription()%>
                </p>
                <div class="row">
                    <div class="small-3 columns">
                        <label class="middle">Brand</label>
                    </div>
                    <div class="small-7 columns">
                        <label class="middle"><%= toy.getBrand()%></label>
                    </div>
                    
                    <% 
                        if(toy.getSecondHandToy() != null){
                            String status = toy.getSecondHandToy().getStatus();
                            String label = "";
                            
                            if(status.equals(SecondHandToy.SOLD))
                                label = "success";
                            else if(status.equals(SecondHandToy.WAIT_APPROVE))
                                label = "warning";
                            else label = "";
                    %>
                    
                    <div class="small-2 columns">
                            <label class="middle"><span class="<%= label %> label"><%= status %></span></label>
                    </div>
                    <%   
                        }

                        boolean disableBuy = false;

                        if(toy.getSecondHandToy() == null){
                            // is new toy
                            disableBuy = false;
                        }else{
                            // second hand toy
                            if(toy.getSecondHandToy().getStatus().equals(SecondHandToy.SELLING)){
                                disableBuy = false;
                            }else{
                                disableBuy = true;
                            }
                        }
                    %>
                                       
                    <div class="small-3 columns">
                        <label class="middle">Sex</label>
                    </div>
                    <div class="small-3 columns">
                        <label class="middle"><%= toy.getSex()%></label>
                    </div>
                    <div class="small-3 columns">
                        <label class="middle">Age</label>
                    </div>
                    <div class="small-3 columns">
                        <label class="middle"><%= toy.getAge()%></label>
                    </div>

                    <div class="small-3 columns">
                        <label class="middle">Price</label>
                    </div>
                    <div class="small-3 columns">
                        <%
                            DecimalFormat df = new DecimalFormat("0.00");
                        %>
                        <label class="middle"><%= "$" + df.format(toy.getPrice())%></label>
                    </div>

                    <div class="small-3 columns">
                        <label class="middle">Quantity</label>
                    </div>
                    <div class="small-3 columns">
                        <% if(toy.isSecondHand()){ %>
                            <input type="number" id="qty" min="1" placeholder="Number" value="1" disabled>
                        <% }else{%>
                            <input type="number" id="qty" min="1" placeholder="Number" value="1">
                        <% }%>
                    </div>
                </div>

                <% if(!disableBuy) {%>
                    <div class="large expanded">
                        <a id="addCart" href="cart?action=add&id=<%= toy.getToyID() %>" class="primary button expanded">Add to cart <i class="fi-shopping-cart"></i></a>
                    </div>
                <% }%>    
            </div>
        </div>
        <div class="column row">
            <div class="callout">
                <h4>Comments&nbsp;<i class="fi-comments"></i></h4>
                <hr>
                <%
                    Vector<Comment> comments = (Vector<Comment>) request.getAttribute("comments");

                    int count = 0;
                    for (Comment cmt : comments) {
                        count++;
                %>
                <div class="row">
                    <div class="columns">
                        <div>
                            <%= "#" + count%>
                            <%= cmt.getContent()%>
                            <%
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                                String date = sdf.format(cmt.getDate());
                            %>
                            <b class="float-right"><%= cmt.getHTMLDisplayName() %></b>    
                        </div>
                        <div>
                            <%
                                String showReply = "";
                                if (cmt.getReplies().size() > 0) {
                                    showReply = "�&nbsp;<a href=\"#\" class=\"showReply\"><i class=\"fi-comments\"></i>&nbspShow ";
                                    if (cmt.getReplies().size() == 1) {
                                        showReply += cmt.getReplies().size() + " Reply";
                                    } else {
                                        showReply += cmt.getReplies().size() + " Replies";
                                    }
                                    showReply += "</a>";
                                }
                                showReply += "&nbsp;�&nbsp;<i class=\"fi-clock\">&nbsp;</i>"+ date ;
                            %>
                            
                            <a href="#" class="reply"><i class="fi-comment">&nbsp;</i>Reply</a>
                            <%= showReply%>
                        </div>
                        <%
                            Vector<Comment> subComments = cmt.getReplies();
                            for (int i = 0; i < subComments.size(); i++) {
                        %>      
                        <div style="padding-left: 25px; display:none" class="replies">
                            <%= subComments.get(i).getContent()%>
                            <b class="float-right">
                                <%= subComments.get(i).getHTMLDisplayName() %>
                            </b>
                            <%
                                date = sdf.format(subComments.get(i).getDate());
                            %>
                            <div>
                                <i class="fi-clock">&nbsp;</i><%= date %>
                            </div>
                        </div>
                        <%
                            }
                        %>    
                        
                        <div class="reply-form" style="display: none">
                            <form method="post" action="comment">
                                <div class="input-group">
                                    <input class="input-group-field" type="text" placeholder="Your reply" name="comment">
                                    <div class="input-group-button">
                                        <input type="submit" class="button" value="Submit Reply">
                                    </div>
                                </div>

                                <input type="hidden" name="reply" value="<%= cmt.getCommentID() %>"/>
                                <input type="hidden" name="action" value="add"/>
                                <input type="hidden" name="toyID" value="<%= toy.getToyID()%>"/>
                            </form>
                        </div>

                        <p></p>
                    </div>
                </div>
                <%
                    }
                %>

                <form method="post" action="comment">
                    <label>
                        My Comment
                        <textarea placeholder="Comment" name="comment"></textarea>
                    </label>
                    <input type="hidden" name="action" value="add"/>
                    <input type="hidden" name="toyID" value="<%= toy.getToyID()%>"/>

                    <input class="button" type="submit" value="Submit Comment"/>
                </form>
            </div>
        </div>
        <div class="row column">
            <hr>
            <jsp:include page="include/footer.html"/>
        </div>
        <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
        <script src="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>

        <script>
            $(document).foundation();

            <%
                String msg = (String) session.getAttribute("msg");
                session.removeAttribute("msg");
            %>
            var msg = "<%= msg%>"

            if (msg != "null")
                alert(msg);
            
            $(document).ready(function(){
                $('.reply').click(function(){
                    var parentDiv = $(this).parent().parent();
                    $('.reply-form').hide();
                    parentDiv.find('.reply-form').show();
                    return false;
                });
                
                $('.showReply').click(function(){
                    var parentDiv = $(this).parent().parent();
                    parentDiv.find('.replies').show();
                    return false;
                });
                
                $('#addCart').click(function(){
                    var _href = $(this).attr("href"); 
                    $(this).attr("href", _href + '&qty=' + $('#qty').val());
                    return true;
                });
            })

        </script>
        <script type="text/javascript" src="https://intercom.zurb.com/scripts/zcom.js"></script>
        
    </body>
</html>
