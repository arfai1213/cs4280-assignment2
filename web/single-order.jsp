<%@page import="util.SessionChecker"%>
<%@page import="bean.Orderline"%>
<%@page import="bean.Order"%>
<%@page import="bean.User"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="bean.Comment"%>
<%@page import="java.util.Vector"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="bean.Toy"%>
<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Toys4Kids | View Order</title>
        <link rel="stylesheet" href="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.min.css">
        <link href='http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/motion-ui/1.1.1/motion-ui.min.css" />

    </head>
    <body>
        <jsp:include page="include/header.jsp"/>

        <br>

        <%
            Order order = (Order) request.getAttribute("order");
        %>

        <div class="row columns">
            <nav aria-label="You are here:" role="navigation">
                <ul class="breadcrumbs">
                    <li><a href="#">Home</a></li>
                    <li>
                        <span class="show-for-sr">Current: </span> ORDER
                    </li>
                </ul>
            </nav>
        </div>

        <%
            String msg = (String) session.getAttribute("msg");
            session.removeAttribute("msg");

            if (msg != null && !msg.equals("")) {
        %>

        <div class="row">
            <div class="success callout" data-closable="slide-out-right">
                <h5><%= msg %></h5>
                <p>Here is the order detail!</p>
                <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        <% }%>
        
        <% 
            User loginUser = SessionChecker.getCurrentUser(session);
            if(loginUser.isCustomer() && order.getOrderBy() != loginUser.getUserID()){
                //check if the customer have access to this order
        %>
        <div class="row columns">
            <h3>You have no permission to access this order data (order ID: <%= order.getOrderID() %> )</h3>
        </div>

        <%
                }else{
        %>
        <div class="row columns">            
            <div class="columns small-12 large-3">Order ID:&nbsp;#<%= order.getOrderID()%></div>
            <div class="columns small-12 large-3">Order status:&nbsp;<span class="success label">Paid</span></div>
            <div class="columns small-12 large-3">Order by:&nbsp;<%= order.getOrderByUser().getDisplayName()%></div>
            <div class="columns small-12 large-3">Total amount:&nbsp;HKD$<%= order.getTotal()%></div>
            <div class="columns small-12">Delivery address:&nbsp;<%= order.getAddr()%></div>    
        </div>

        <br/>
        <div class="row columns">
            <table class="stack">
                <thead>
                <th>Product</th>
                <th>Price</th>
                <th>Quantity</th>
                <th>Total</th>
                <th></th>
                </thead>
                <tbody>
                    <%
                        Vector<Orderline> orderlist = order.getOrderlines();
                        for (int i = 0; i < orderlist.size(); i++) {
                            Orderline ol = orderlist.get(i);
                            Toy toy = ol.getToy();
                    %>
                    <tr>
                        <td>
                            <div class="row">
                                <div class="column medium-2 large-2">
                                    <img src="<%= toy.getImage()%>" class="thumbnail">
                                </div>
                                <div class="column medium-10 large-10">
                                    <h5><%= toy.getHeader()%></h5>
                                    <br/>
                                    <div><%= toy.getBrand()%></div>
                                    <div><%= toy.getSex()%></div>
                                    <div><%= toy.getAge()%></div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <%= toy.getPrice()%>
                        </td>
                        <td><%= ol.getQty()%></td>
                        <td>$<%= toy.getPrice() * ol.getQty()%></td>
                    </tr>

                    <% }%>
                </tbody>
            </table>
        </div>

        <%
            }
        %>

        <div class="row columns">
            <jsp:include page="include/footer.html"/>
        </div>

        <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
        <script src="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>
        <script type="text/javascript" src="https://intercom.zurb.com/scripts/zcom.js"></script>

        <script>
            $(document).foundation();
        </script>

    </body>
</html>
