<%@page import="bean.User"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="bean.Comment"%>
<%@page import="java.util.Vector"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="bean.Toy"%>
<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Toys4Kids | Register</title>
        <link href="style/foundation.css" rel="stylesheet">

        <link rel="stylesheet" href="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.min.css">
        <link href='http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>
        <link href="style/main.css" rel="stylesheet">

    </head>
    <body>
        <div>
            <jsp:include page="include/header.jsp"/>
                    <br>

        <hr>
        <div class="row column text-center" style="max-width: 800px; margin-left: auto; margin-right: auto; ">
            <h2>Register</h2>
            <hr class="header-hr">
        </div>
        <form action="registerservlet" method="POST" style="max-width: 800px; margin-left: auto; margin-right: auto; ">
            <div class="row">
               <div class="large-6 columns">
                 <div class="row collapse prefix-radius">
                   <div class="small-3 columns">
                     <span class="prefix">Your Name</span>
                   </div>
                   <div class="small-9 columns">
                       <input type="text" placeholder="Your Name" name="fullname" reqiured/>
                   </div>
                 </div>
                </div>
               <div class="large-6 columns">
                 <div class="row collapse prefix-radius">
                   <div class="small-3 columns">
                     <span class="prefix">Username</span>
                   </div>
                   <div class="small-9 columns">
                       <input type="text" placeholder="Username" name="username" reqiured/>
                   </div>
                 </div>
               </div>
            </div>
            
            <div class="row">
               <div class="large-6 columns">
                 <div class="row collapse prefix-radius">
                   <div class="small-3 columns">
                     <span class="prefix">Password</span>
                   </div>
                   <div class="small-9 columns">
                       <input type="password" placeholder="Password" name="password" required/>
                   </div>
                 </div>
                </div>
               <div class="large-6 columns">
                 <div class="row collapse prefix-radius">
                   <div class="small-3 columns">
                     <span class="prefix">Retype password</span>
                   </div>
                   <div class="small-9 columns">
            <input type="password" placeholder="Enter Your Password Again" name="password-retype" required/>
                   </div>
                 </div>
               </div>
            </div>
            <div class="row">
                          <div class="large-12 columns">
                            <label>Address
                                <textarea name="address" required style="min-height: 40px;"></textarea>
                            </label>
                          </div>
            </div>
                        <input type="hidden" value="register" name="action" />

                        <div class="row">
                            <div class="large-12 columns">
                                <input class="primary button"  style="width: 100%;" type="submit" value="Register" />
                            </div>
                        </div>
            
        </form>
            
            
            
        <div class="row column">
            <hr>
            <jsp:include page="include/footer.html"/>
        </div>
        <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
        <script src="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>

        <script>
            $(document).foundation();

            <%
                String msg = (String) session.getAttribute("msg");
                session.removeAttribute("msg");
            %>
            var msg = "<%= msg%>"

            if (msg != "null")
                alert(msg);
            
            $(document).ready(function(){
                $('.reply').click(function(){
                    var parentDiv = $(this).parent().parent();
                    $('.reply-form').hide();
                    parentDiv.find('.reply-form').show();
                    return false;
                });
                
                $('.showReply').click(function(){
                    var parentDiv = $(this).parent().parent();
                    parentDiv.find('.replies').show();
                    return false;
                });
                
                $('#addCart').click(function(){
                    var _href = $(this).attr("href"); 
                    $(this).attr("href", _href + '&qty=' + $('#qty').val());
                    return true;
                });
            })

        </script>
        <script type="text/javascript" src="https://intercom.zurb.com/scripts/zcom.js"></script>
        
    </body>
</html>
