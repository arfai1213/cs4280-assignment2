<%@page import="bean.User"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="bean.Comment"%>
<%@page import="java.util.Vector"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="bean.Toy"%>
<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Toys4Kids | Maintain Toys</title>
        <link rel="stylesheet" href="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.min.css">
        <link href='http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="./style/main.css">
    </head>
    <body>
        <jsp:include page="include/header.jsp"/>
        
        <br>

        <%
            String action = (String) request.getAttribute("action");
            session = request.getSession();
        %>
        
        <% if(action.equals("view")) { 
            Vector<Toy> toys = (Vector<Toy>) request.getAttribute("toys");
        %>
        <div class="row columns">
            <nav aria-label="You are here:" role="navigation">
                <ul class="breadcrumbs">
                    <li><a href="home">Home</a></li>
                    <li>
                        <span class="show-for-sr">Current: </span> Maintain Toys
                    </li>
                </ul>
            </nav>
        </div>
        <hr>
        <div class="row column text-center">
            <h2>Maintain Toys</h2>
            <hr class="header-hr">
        </div>
        <div class="row small-up-2 large-up-4">

        <a href="create-toy" class="primary button" style="margin: 0 15px 30px 15px;">Create New Toy</a>
        </div>
            <div class="row small-up-2 large-up-4">
                <%
                    for(int i = 0; i<toys.size(); i++ ) { 
                        if(toys.size()<=i) break;
                        if (i!=0 && (i)%4==0) {
                            out.print("</div> <div class=\"row small-up-2 large-up-4\">");
                        }
                        Toy toy = toys.get(i);
                        DecimalFormat df = new DecimalFormat("0.00");

                %>
                    <div class="column">
                    <div class="thumbnail item-image-container <%=toy.isSecondHand()?"second-hand-label":"" %>" style="background-image: url(<%=toy.getImage() %>); "></div>
                    <div class="header-container">
                        <h5><%=toy.getHeader() %></h5>
                    </div>
                    <div class="row">
                        <div class="small-4 columns">
                            <label class="smail">Brand</label>
                        </div>
                        <div class=" data-text">
                            <label class="smail"><%= toy.getBrand()%></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-4 columns">
                            <label class="smail">Price</label>
                        </div>
                        <div class=" data-text">
                            <label class="smail"><%= "$" + df.format(toy.getPrice())%></label>
                        </div>
                    </div>
                    <hr class="header-hr">
                    <div class="row button-row">
                    <a href="edit-toy?id=<%= toy.getToyID() %>" class="hollow button">Edit</a>
                    <a href="maintain-toys?action=remove&id=<%= toy.getToyID() %>" class="alert button" onclick="return confirm('Are you sure to remove this item?')">Remove</a>
                    </div>
                    </div> 

                <%  } %>     
                </div>   

        <% } %>
        <div class="row column">
            <hr>            
            <jsp:include page="include/footer.html"/>
        </div>
        <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
        <script src="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>

        <script>
            $(document).foundation();

            <%
                String msg = (String) session.getAttribute("msg");
                session.removeAttribute("msg");
            %>
            var msg = "<%= msg%>";

            if (msg != "null")
                alert(msg);
            
            $(document).ready(function(){
                $('.reply').click(function(){
                    var parentDiv = $(this).parent().parent();
                    $('.reply-form').hide();
                    parentDiv.find('.reply-form').show();
                    return false;
                });
                
                $('.showReply').click(function(){
                    var parentDiv = $(this).parent().parent();
                    parentDiv.find('.replies').show();
                    return false;
                });
                
                $('#addCart').click(function(){
                    var _href = $(this).attr("href"); 
                    $(this).attr("href", _href + '&qty=' + $('#qty').val());
                    return true;
                });
            })

        </script>
        <script type="text/javascript" src="https://intercom.zurb.com/scripts/zcom.js"></script>
        
    </body>
</html>
