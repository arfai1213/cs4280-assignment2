<%@page import="bean.User"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="bean.Comment"%>
<%@page import="java.util.Vector"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="bean.Toy"%>
<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Toys4Kids | Browse</title>
        <link rel="stylesheet" href="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.min.css">
        <link href='http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="./style/main.css">
    </head>
    <body>
        <jsp:include page="include/header.jsp"/>
        
        <br>

        <%
            Vector<Toy> toys = (Vector<Toy>) request.getAttribute("toys");
            String category = (String) request.getAttribute("category");
            String brand = (String) request.getAttribute("brand");
            String gender = (String) request.getAttribute("gender");
            String age = (String) request.getAttribute("age");
            String price = (String) request.getAttribute("price");
            Vector<String> brands = (Vector<String>) request.getAttribute("brands");
            session = request.getSession();
        %>
        
        <div class="row columns">
            <nav aria-label="You are here:" role="navigation">
                <ul class="breadcrumbs">
                    <li><a href="home">Home</a></li>
                    <li>
                        <span class="show-for-sr">Current: </span> <%=category %>
                    </li>
                </ul>
            </nav>
        </div>
        <hr>
        <div class="row column text-center">
            <h2><%=category %></h2>
            <hr class="header-hr">
        </div>
        <div class="row small-up-2 large-up-4">
            <table style="margin-left:auto; margin-right:auto;">
                <tr>
                    <td>Brand: </td>
                    <td><a href="?" <%=brand==null?"class=\"active\"":"" %>>ALL</a></td>
                    <%
                        for(int i = 0; i<brands.size(); i++ ) { 
                            String lower = brands.get(i).toLowerCase();
                            String upper = brands.get(i).toUpperCase();
                    %>
                    <td><a href="?brand=<%=lower%>" <%=brand!=null && brand.equals(lower)?"class=\"active\"":"" %>><%=upper%></a></td>
                    <%
                        }
                    %>
                </tr>
                <tr>
                    <td>Gender </td>
                    <td><a href="?" <%=gender==null?"class=\"active\"":"" %>>ALL</a></td>
                    <td><a href="?gender=m" <%=gender!=null && gender.equals("m")?"class=\"active\"":"" %>>FOR BOYS</a></td>
                    <td><a href="?gender=f" <%=gender!=null && gender.equals("f")?"class=\"active\"":"" %>>FOR GIRLS</a></td>
                </tr>
                <tr>
                    <td>Age</td>
                    <td><a href="?" <%=age==null?"class=\"active\"":"" %>>ALL</a></td>
                    <td><a href="?age=0" <%=age!=null && age.equals("0")?"class=\"active\"":"" %>>0 ~ 3</a></td>
                    <td><a href="?age=4" <%=age!=null && age.equals("4")?"class=\"active\"":"" %>>4 ~7</a></td>
                    <td><a href="?age=8" <%=age!=null && age.equals("8")?"class=\"active\"":"" %>>8 ~ 11</a></td>
                    <td><a href="?age=12" <%=age!=null && age.equals("12")?"class=\"active\"":"" %>>12 ~ 15</a></td>
                    <td><a href="?age=16" <%=age!=null && age.equals("16")?"class=\"active\"":"" %>>16 ~ 18</a></td>
                </tr>
                <tr>
                    <td>Price</td>
                    <td><a href="?" <%=price==null?"class=\"active\"":"" %>>ALL</a></td>
                    <td><a href="?price=0" <%=price!=null && price.equals("0")?"class=\"active\"":"" %>>Below $50</a></td>
                    <td><a href="?price=50" <%=price!=null && price.equals("50")?"class=\"active\"":"" %>>$50 ~ $99</a></td>
                    <td><a href="?price=100" <%=price!=null && price.equals("100")?"class=\"active\"":"" %>>$100 ~ $199</a></td>
                    <td><a href="?price=200" <%=price!=null && price.equals("200")?"class=\"active\"":"" %>>$200 ~ $499</a></td>
                    <td><a href="?price=500" <%=price!=null && price.equals("500")?"class=\"active\"":"" %>>$500 or Above</a></td>
                </tr>
            </table>
        </div>
        <div class="row small-up-2 large-up-4">
        <%
            for(int i = 0; i<toys.size(); i++ ) { 
                if(toys.size()<=i) break;
                if (i!=0 && (i)%4==0) {
                    out.print("</div> <div class=\"row small-up-2 large-up-4\">");
                }
                Toy toy = toys.get(i);
                DecimalFormat df = new DecimalFormat("0.00");
                
        %>
                    <div class="column">
                    <div class="thumbnail item-image-container <%=toy.isSecondHand()?"second-hand-label":"" %>" style="background-image: url(<%=toy.getImage() %>); "></div>
                    <div class="header-container">
                        <h5><%=toy.getHeader() %></h5>
                    </div>
                    <div class="row">
                        <div class="small-4 columns">
                            <label class="smail">Brand</label>
                        </div>
                        <div class=" data-text">
                            <label class="smail"><%= toy.getBrand()%></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-4 columns">
                            <label class="smail">Price</label>
                        </div>
                        <div class=" data-text">
                            <label class="smail"><%= "$" + df.format(toy.getPrice())%></label>
                        </div>
                    </div>
                    <hr class="header-hr">
                    <div class="row button-row">
                        <a id="addCart" href="cart?action=add&id=<%= toy.getToyID() %>&qty=1" class="primary button">Add to cart <i class="fi-shopping-cart"></i></a>
                        <a class="secondary button" href="toy?id=<%= toy.getToyID() %>">Details</a>
                    </div>
                    </div> 
                    
        <%  } %>     
        </div>       
                    
        <div class="row column">
            <hr>
            <jsp:include page="include/footer.html"/>
        </div>
        <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
        <script src="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>

        <script>
            $(document).foundation();

            <%
                String msg = (String) session.getAttribute("msg");
                session.removeAttribute("msg");
            %>
            var msg = "<%= msg%>"

            if (msg != "null")
                alert(msg);
            
            $(document).ready(function(){
                $('.reply').click(function(){
                    var parentDiv = $(this).parent().parent();
                    $('.reply-form').hide();
                    parentDiv.find('.reply-form').show();
                    return false;
                });
                
                $('.showReply').click(function(){
                    var parentDiv = $(this).parent().parent();
                    parentDiv.find('.replies').show();
                    return false;
                });
                
                $('#addCart').click(function(){
                    var _href = $(this).attr("href"); 
                    $(this).attr("href", _href + '&qty=' + $('#qty').val());
                    return true;
                });
            })

        </script>
        <script type="text/javascript" src="https://intercom.zurb.com/scripts/zcom.js"></script>
        
    </body>
</html>
