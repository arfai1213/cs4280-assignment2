<%@page import="bean.User"%>
<%@page import="util.SessionChecker"%>
<%@page import="bean.Toy"%>
<%@page import="java.util.ArrayList"%>
<%@page import="bean.Orderline"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Toys4Kids | Shopping Cart</title>
        <link rel="stylesheet" href="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.min.css">
        <link href='http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>

    </head>
    <body>
        <jsp:include page="include/header.jsp"/>

        <br/>
        <div class="row columns">
            <h4>
                <i class="fi-shopping-cart">&nbsp;</i>Your Cart
            </h4>
        </div>

        <div class="row">
            <div class="columns large-12">

                <%
                    ArrayList<Orderline> orderlist = (ArrayList<Orderline>) session.getAttribute("cart");

                    if (orderlist != null && orderlist.size() > 0) {
                %>
                <table class="hover columns">
                    <thead>
                        <tr>
                            <th>Product</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Total</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <%
                            double total = 0;
                            for (int i = 0; i < orderlist.size(); i++) {
                                Orderline ol = orderlist.get(i);
                                Toy toy = ol.getToy();
                                total += toy.getPrice() * ol.getQty();
                        %>
                        <tr>
                            <td>
                                <div class="row">
                                    <div class="column medium-2 large-2">
                                        <img src="<%= toy.getImage()%>" class="thumbnail">
                                    </div>
                                    <div class="column medium-10 large-10">
                                        <h5><%= toy.getHeader()%></h5>
                                        <br/>
                                        
                                        <% if(toy.getSecondHandToy() != null){ %>
                                        <div><span class="label secondary">Second hand toy</span></div>
                                        <% }%>
                                        <div><%= toy.getBrand()%></div>
                                        <div><%= toy.getSex()%></div>
                                        <div><%= toy.getAge()%></div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <%= toy.getPrice()%>
                            </td>
                            <td><%= ol.getQty()%></td>
                            <td>$<%= toy.getPrice() * ol.getQty()%></td>
                            <td><a href="cart?action=remove&id=<%= toy.getToyID() %>">Remove</a></td>
                        </tr>

                        <% } %>
                    </tbody>
                </table>
                <hr/>
                <div class="row">
                    <div class="column medium-12 large-12">
                        <div class="float-right">Cart subtotal<b style="padding-left: 15px">HKD$<%= total %></b></div>
                    </div>
                </div>
                <div class="row">
                    <div class="column medium-12 large-12">
                        <div class="float-right"><b style="padding-left: 15px">Free shipping</b></div>
                    </div>
                    <div class="column medium-12 large-12">
                        <form method="POST" action="cart">
                            <%
                                for (int i = 0; i < orderlist.size(); i++) {
                                    Orderline ol = orderlist.get(i);
                                    Toy toy = ol.getToy();
                            %>
                            <input type="hidden" name="toyID[]" value="<%= toy.getToyID()%>"/>
                            <input type="hidden" name="qty[]" value="<%= ol.getQty()%>"/>
                            <%
                                }

                                User loginUser = SessionChecker.getCurrentUser(session);
                                String deliveryAddress = loginUser.getAddress();
                            %>
                            <label>Delivery address
                                <input type="text" id="deliveryAddr" name="deliveryAddr" value="<%= deliveryAddress %>" placeholder="Your delivery address"/>
                            </label>
                            
                            <%
                                if(!SessionChecker.isLogin(session)){
                            %>
                            <div class="column callout warning">
                                <div class="float-right">
                                    Please login to continue
                                </div>
                            </div>
                            <%
                                }
                            %>
                            <div class="float-right">
                                <%
                                    if(!SessionChecker.isLogin(session)){
                                %>
                                    <input type="submit" value="Checkout" class="button float-right" disabled/>
                                <%
                                    }else{
                                %>
                                    <input type="submit" value="Checkout" class="button float-right"/>
                                <% } %>
                            </div>
                        </form>
                    </div>
                </div>

                <%
                    } else {
                %>
                <h5>You have no item in shopping cart</h5>
                <% }%>
            </div>
        </div>
    </body>

    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>
    <script type="text/javascript" src="https://intercom.zurb.com/scripts/zcom.js"></script>
    
    <script>
        $(document).foundation();
    </script>
</html>
