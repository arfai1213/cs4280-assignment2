<%@page import="java.util.Collections"%>
<%@page import="bean.User"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="bean.Comment"%>
<%@page import="java.util.Vector"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="bean.Toy"%>
<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Toys4Kids | Home</title>
        <link rel="stylesheet" href="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.min.css">
        <link rel="stylesheet" href="./style/main.css">
        <link href='http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>

    </head>
    <body>
        <jsp:include page="include/header.jsp"/>
        <div class="orbit" role="region" aria-label="Favorite Space Pictures" data-orbit>
        <ul class="orbit-container">
        <button class="orbit-previous" aria-label="previous"><span class="show-for-sr">Previous Slide</span>&#9664;</button>
        <button class="orbit-next" aria-label="next"><span class="show-for-sr">Next Slide</span>&#9654;</button>
        <%
            Vector<Toy> toys = (Vector<Toy>) request.getAttribute("toys");
            Vector<Toy> firstHandToys = (Vector<Toy>) request.getAttribute("firstHandToys");
            Vector<Toy> secondHandToys = (Vector<Toy>) request.getAttribute("secondHandToys");

            //session = request.getSession();
            for(Toy toy : toys ) { 
        %>
            <li class="orbit-slide is-active">
                <img src="" style="background: url(<%=toy.getImage() %>); background-position: center; background-size:cover; height: 350px; width:100%;">
            </li>
        <% } %>
        </ul>
        </div>
        <hr>
        <div class="row column text-center">
            <h2>New Toys</h2>
            <hr class="header-hr">
        </div>
        
        <div class="row small-up-2 large-up-4">
        <%
            Collections.shuffle(toys);
            for(int i = 0; i<4; i++ ) { 
                if(toys.size()<=i) break;
                Toy toy = toys.get(i);
                DecimalFormat df = new DecimalFormat("0.00");
        %>
        <div class="column">
        <div class="thumbnail item-image-container <%=toy.isSecondHand()?"second-hand-label":"" %>" style="background-image: url(<%=toy.getImage() %>); "></div>
        <div class="header-container">
            <h5><%=toy.getHeader() %></h5>
        </div>
            <p><%= "$" + df.format(toy.getPrice())%></p>
        <a href="./toy?id=<%=toy.getToyID() %>" class="button expanded">Buy</a>
        </div>
        <% } %>
        </div>
        <hr>
        <div class="row column">
            <div class="callout primary">
                <h3>Really big special this week on items.</h3>
            </div>
        </div>
        <hr>
        <div class="row column text-center">
            <h2>Our New Toys</h2>
            <hr class="header-hr">
        </div>
        <div class="row small-up-2 medium-up-3 large-up-6">

        <%
            Collections.shuffle(firstHandToys);
            for(int i = 0; i<6; i++ ) { 
                if(firstHandToys.size()<=i) break;
                Toy toy = firstHandToys.get(i);
                DecimalFormat df = new DecimalFormat("0.00");
        %>
        <div class="column">
        <div class="thumbnail item-image-container" style="background-image: url(<%=toy.getImage() %>); "></div>
        <div class="header-container">
            <h5><%=toy.getHeader() %></h5>
        </div>
        <p><%= "$" + df.format(toy.getPrice())%></p>
        <a href="./toy?id=<%=toy.getToyID() %>" class="button small expanded hollow">Buy</a>
        </div>
        <% } %>
        </div>
        <hr>
        <div class="row column text-center">
            <h2>Some Other Recycled Toys</h2>
            <hr class="header-hr">
        </div>
        <div class="row small-up-2 medium-up-3 large-up-6">

        <%
            Collections.shuffle(secondHandToys);
            for(int i = 0; i<6; i++ ) { 
                if(secondHandToys.size()<=i) break;
                Toy toy = secondHandToys.get(i);
                DecimalFormat df = new DecimalFormat("0.00");
        %>
        <div class="column">
        <div class="thumbnail item-image-container second-hand-label" style="background-image: url(<%= toy.getImage() %>); "></div>
        <div class="header-container">
            <h5><%=toy.getHeader() %></h5>
        </div>
        <p><%= "$" + df.format(toy.getPrice())%></p>
        <a href="./toy?id=<%=toy.getToyID() %>" class="button small expanded hollow">Buy</a>
        </div>
        <% } %>
        </div>
        
        <div class="row column">
            <hr>            
            <jsp:include page="include/footer.html"/>
        </div>
        <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
        <script src="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>
        <script>
            $(document).foundation();

            <%
                String msg = (String) session.getAttribute("msg");
                session.removeAttribute("msg");
            %>
            var msg = "<%= msg%>"

            if (msg != "null")
                alert(msg);
            
            $(document).ready(function(){
                $('.reply').click(function(){
                    var parentDiv = $(this).parent().parent();
                    $('.reply-form').hide();
                    parentDiv.find('.reply-form').show();
                    return false;
                });
                
                $('.showReply').click(function(){
                    var parentDiv = $(this).parent().parent();
                    parentDiv.find('.replies').show();
                    return false;
                });
                
                $('#addCart').click(function(){
                    var _href = $(this).attr("href"); 
                    $(this).attr("href", _href + '&qty=' + $('#qty').val());
                    return true;
                });
            })

        </script>
        <script type="text/javascript" src="https://intercom.zurb.com/scripts/zcom.js"></script>
        
    </body>
</html>
