<%@page import="util.SessionChecker"%>
<%@page import="bean.SecondHandToy"%>
<%@page import="java.util.Map"%>
<%@page import="bean.Orderline"%>
<%@page import="bean.Order"%>
<%@page import="bean.User"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="bean.Comment"%>
<%@page import="java.util.Vector"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="bean.Toy"%>
<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Toys4Kids | Recycled toys request</title>
        <link rel="stylesheet" href="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.min.css">
        <link href='http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/motion-ui/1.1.1/motion-ui.min.css" />
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css" />
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/dataTables.foundation.min.css" />

    </head>
    <body>
        <jsp:include page="include/header.jsp"/>

        <br>

        <div class="row columns">
            <nav aria-label="You are here:" role="navigation">
                <ul class="breadcrumbs">
                    <li><a href="home">Home</a></li>
                    <li>
                        <span class="show-for-sr">Current: </span> Recycle toy status
                    </li>
                </ul>
            </nav>
        </div>

        <div class="row columns">
            <div class="columns small-12 large-12">
                <table id="2ndRequest" class="display">
                    <thead>
                    <th>Toy ID</th>
                    <th>Toy Name</th>
                    <th>Price</th>
                    <th>Created at</th>
                    <th>Status</th>
                    <th></th>
                    </thead>
                    <tbody>
                        <%
                            Vector<SecondHandToy> all2ndToys = (Vector<SecondHandToy>) request.getAttribute("SecondHandToysRequest");
                            if (all2ndToys != null) {
                                for (int i = 0; i < all2ndToys.size(); i++) {
                        %>
                        <%
                            SecondHandToy secondToy = all2ndToys.get(i);
                        %>
                        <tr>
                            <td><%= secondToy.getToyID()%></td>
                            <td><%= secondToy.getToy().getHeader() %></td>
                            <td><%= secondToy.getToy().getPrice() %></td>
                            <td><%= new SimpleDateFormat("yyyy-MM-dd").format(secondToy.getCreateDate()).toString() %></td>
                            <td>
                                <% 
                                    String label = "";
                                    String status = secondToy.getStatus();
                                    
                                    if(status.equals(SecondHandToy.SOLD))
                                        label = "success";
                                    else if(status.equals(SecondHandToy.WAIT_APPROVE))
                                        label = "warning";
                                    else label = "";
                                %>
                                <span class="<%= label %> label"><%= secondToy.getStatus() %></span>
                            </td>
                            <td>
                                <a href="toy?id=<%= secondToy.getToyID() %>">View</a>
                                <% 
                                    User loginUser = SessionChecker.getCurrentUser(session);
                                    if(!loginUser.isCustomer() && secondToy.getStatus().equals(SecondHandToy.WAIT_APPROVE)){
                                %>
                                    &nbsp;
                                    <a href="approve-2nd-hand-toys?action=approve&id=<%= secondToy.getToyID() %>">Approve</a>
                                <% 
                                    }
                                %>
                            </td>
                        </tr>
                        <%      }

                            }%>
                    </tbody>
                </table>
            </div>
        </div>


        <div class="row columns">
            <jsp:include page="include/footer.html"/>
        </div>

        <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
        <script src="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>
        <script type="text/javascript" src="https://intercom.zurb.com/scripts/zcom.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.11/js/dataTables.foundation.min.js"></script>

        <script>
            $(document).foundation();
            $(document).ready(function () {
                $('#2ndRequest').DataTable({
                    "dom": 'frtip',
                    "language": {
                        "emptyTable": "Your have no recycle toy requested!"
                    }
                });
            });
        </script>

    </body>
</html>
