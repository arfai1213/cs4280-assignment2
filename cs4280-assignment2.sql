USE [aiad039_db]
GO
/****** Object:  Table [dbo].[comment]    Script Date: 4/29/2016 2:32:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[comment](
	[commentID] [int] IDENTITY(1,1) NOT NULL,
	[content] [text] NOT NULL,
	[toyID] [int] NOT NULL,
	[replyTo] [int] NULL,
	[date] [datetime] NOT NULL,
	[userID] [int] NULL,
 CONSTRAINT [PK_comment] PRIMARY KEY CLUSTERED 
(
	[commentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[order]    Script Date: 4/29/2016 2:32:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[order](
	[orderID] [int] IDENTITY(1,1) NOT NULL,
	[orderDate] [datetime] NOT NULL,
	[deliveryAddr] [text] NOT NULL,
	[orderBy] [int] NOT NULL,
 CONSTRAINT [PK_order] PRIMARY KEY CLUSTERED 
(
	[orderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[orderline]    Script Date: 4/29/2016 2:32:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[orderline](
	[orderID] [int] NOT NULL,
	[toyID] [int] NOT NULL,
	[qty] [int] NOT NULL,
	[subtotal] [decimal](20, 2) NOT NULL,
 CONSTRAINT [PK_orderline] PRIMARY KEY CLUSTERED 
(
	[orderID] ASC,
	[toyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[secondhandtoy]    Script Date: 4/29/2016 2:32:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[secondhandtoy](
	[ToyID] [int] NOT NULL,
	[approved] [bit] NOT NULL,
	[createDate] [datetime] NULL,
	[soldBy] [int] NULL,
	[isSold] [bit] NOT NULL,
 CONSTRAINT [PK_secondhandtoy] PRIMARY KEY CLUSTERED 
(
	[ToyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[toy]    Script Date: 4/29/2016 2:32:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[toy](
	[toyID] [int] IDENTITY(1,1) NOT NULL,
	[header] [text] NOT NULL,
	[description] [text] NOT NULL,
	[sex] [nvarchar](50) NOT NULL,
	[age] [nvarchar](50) NULL,
	[brand] [nvarchar](255) NOT NULL,
	[price] [decimal](10, 2) NOT NULL,
	[image] [nvarchar](255) NULL,
	[isDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_toy] PRIMARY KEY CLUSTERED 
(
	[toyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[user]    Script Date: 4/29/2016 2:32:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user](
	[userID] [int] IDENTITY(1,1) NOT NULL,
	[username] [nvarchar](50) NOT NULL,
	[password] [nvarchar](50) NOT NULL,
	[displayName] [nvarchar](50) NOT NULL,
	[address] [nvarchar](255) NOT NULL,
	[role] [nvarchar](10) NOT NULL,
	[credit] [decimal](20, 2) NOT NULL,
 CONSTRAINT [PK_user] PRIMARY KEY CLUSTERED 
(
	[userID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[secondhandtoy] ADD  CONSTRAINT [DF__secondhan__isSol__628FA481]  DEFAULT (0) FOR [isSold]
GO
ALTER TABLE [dbo].[toy] ADD  DEFAULT (0) FOR [isDeleted]
GO
ALTER TABLE [dbo].[user] ADD  CONSTRAINT [DF_user_credit]  DEFAULT (0) FOR [credit]
GO
ALTER TABLE [dbo].[comment]  WITH NOCHECK ADD  CONSTRAINT [FK_comment_comment] FOREIGN KEY([replyTo])
REFERENCES [dbo].[comment] ([commentID])
GO
ALTER TABLE [dbo].[comment] CHECK CONSTRAINT [FK_comment_comment]
GO
ALTER TABLE [dbo].[comment]  WITH NOCHECK ADD  CONSTRAINT [FK_comment_toy] FOREIGN KEY([toyID])
REFERENCES [dbo].[toy] ([toyID])
GO
ALTER TABLE [dbo].[comment] CHECK CONSTRAINT [FK_comment_toy]
GO
ALTER TABLE [dbo].[order]  WITH NOCHECK ADD  CONSTRAINT [FK_order_user] FOREIGN KEY([orderBy])
REFERENCES [dbo].[user] ([userID])
GO
ALTER TABLE [dbo].[order] CHECK CONSTRAINT [FK_order_user]
GO
ALTER TABLE [dbo].[orderline]  WITH CHECK ADD  CONSTRAINT [FK_orderline_toy] FOREIGN KEY([toyID])
REFERENCES [dbo].[toy] ([toyID])
GO
ALTER TABLE [dbo].[orderline] CHECK CONSTRAINT [FK_orderline_toy]
GO
ALTER TABLE [dbo].[secondhandtoy]  WITH CHECK ADD  CONSTRAINT [FK_secondhandtoy_toy] FOREIGN KEY([ToyID])
REFERENCES [dbo].[toy] ([toyID])
GO
ALTER TABLE [dbo].[secondhandtoy] CHECK CONSTRAINT [FK_secondhandtoy_toy]
GO
ALTER TABLE [dbo].[secondhandtoy]  WITH NOCHECK ADD  CONSTRAINT [FK_secondhandtoy_user1] FOREIGN KEY([soldBy])
REFERENCES [dbo].[user] ([userID])
GO
ALTER TABLE [dbo].[secondhandtoy] CHECK CONSTRAINT [FK_secondhandtoy_user1]
GO
